/* f3dtest.c
 * This is a simple C test of f3d functionality
 *
 * This file is part of an f3d package, which was designed and partialy also
 * implemented during the author's post doctoral stay at SUNY an Stony Brook,
 * in the lab of Prof. Arie Kaufman.
 * 
 * For details, see the f3d World-Wide-Web page,
 * http://www.cs.sunysb.edu/~vislab/vxtools
 * or send a mail to milos.sramek@oeaw.ac.at
 * 
 * Copyright (C) 1999 Free Software Foundation
 * Milos Sramek  <milos.sramek@oeaw.ac.at>
 * 
 * f3d package is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * the f3d package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with the f3d package -- see the file COPYING.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <f3d.h>

#define size 30

#define  F3D_MONO_TEST(type, typeId, typeName, ctype) 	 	\
{								\
	char	*name = "f3dTest.f3d";				\
	int	i;						\
	char	*comm[3]={"Test grid", "12345 ASDF"};		\
	int 	nx, ny, nz;					\
	int 	nBands2;					\
	f3dDataType 	dType2;					\
	char 	**comm2;					\
	static 	type val = (type) 92.321,		 	\
		    g1[size*size*size],				\
		    *g2;					\
	comm[2] = malloc(1000);					\
	for(i=0; i<1000; i++) comm[2][i] = 'A'; comm[2][999]='\0';\
								\
	for(i=0; i<size*size*size; i++) g1[i] = i%91;		\
	g1[size*size*size / 10] = val;				\
	g1[size*size*size / 10+1] = val;			\
	g1[size*size*size / 10+2] = val;			\
	g1[size*size*size / 2] = val;				\
	f3dHeader hdr = f3dDefaultHeader(size, size, size, typeId, 1); \
	hdr.comptype = ctype;					\
	f3dSetHdrComment(&hdr, 3, comm);			\
								\
	f3dWriteGrid(name, g1, &hdr);				\
								\
	f3dHeader hdr2;						\
	f3dReadGrid(name,(void **)&g2, &hdr2);			\
								\
	if(hdr2.nbands != hdr.nbands || hdr2.dtype != hdr.dtype){	\
		fprintf(stderr,"%s: Reading voxel type failed!!\n", typeName);\
		exit(-1);					\
	}							\
								\
	if(hdr2.comptype != hdr.comptype){			\
		fprintf(stderr,"%s: Compression type failed!!\n", typeName);\
		exit(-1);					\
	}							\
								\
	if(hdr.nx != size || hdr.ny != size || hdr.nz != size){		\
		fprintf(stderr,"%s: Reading dimensions failed!!\n", typeName);\
		exit(-1);					\
	}							\
								\
	if(hdr2.commentLines != hdr.commentLines){		\
		fprintf(stderr,"%s: Reading comment line number failed!!\n", \
				typeName);			\
		exit(-1);					\
	}							\
								\
	for(i=0;i<hdr.commentLines; i++)			\
		if(strcmp(hdr.comment[i], hdr2.comment[i]) != 0){\
			fprintf(stderr,				\
				"%s: Reading comment line %d failed!!\n", \
				typeName, i);			\
			fprintf(stderr,	"Written: \n%s\n", hdr.comment[i]);\
			fprintf(stderr,	"Read: \n%s\n", hdr2.comment[i]);\
			exit(-1);				\
		}						\
								\
	if (g1[size*size*size / 2] != g2[size*size*size / 2]){	\
		fprintf(stderr,"%s: Reading data failed!!", typeName);\
		exit(-1);					\
	}							\
	fprintf(stderr, "Testing %s with compression type %d passed\n", typeName, ctype); 							\
}

#define MONO_TEST(ctype)						\
	F3D_MONO_TEST(f3dUChar, f3dUCharType, "f3dUChar", ctype)	\
	F3D_MONO_TEST(f3dChar, f3dCharType, "f3dChar", ctype)		\
	F3D_MONO_TEST(f3dUInt16, f3dUInt16Type, "f3dUInt16", ctype)	\
	F3D_MONO_TEST(f3dUInt12, f3dUInt12Type, "f3dUInt12", ctype)	\
	F3D_MONO_TEST(f3dInt16, f3dInt16Type, "f3dInt16", ctype)	\
	F3D_MONO_TEST(f3dIntHU, f3dIntHUType, "f3dIntHU", ctype)	\
	F3D_MONO_TEST(f3dUInt32, f3dUInt32Type, "f3dUInt32", ctype)	\
	F3D_MONO_TEST(f3dInt32, f3dInt32Type, "f3dInt32", ctype)	\
	F3D_MONO_TEST(f3dFloat, f3dFloatType, "f3dFloat", ctype)


main(int argc, char *argv[])
{
	MONO_TEST(f3dCompZlib)
	MONO_TEST(f3dCompNone)

	return 0;
}

/**@name A simple test of {\bf f3d} functionality
This is a simple test of the {\tt f3dWriteCubGrid}
and {\tt f3dReadCubGrid} functions for different voxel types.

For each voxel type, the program 
\begin{enumerate}
\item Creates a grid and sets one voxel to some arbitrary value,
\item Stores the grid to a file in {\bf f3d} format,
\item Reads the grid from the file,
\item Checks some data field from the header, and finaly
\item Compares the read value of the voxel with the original value
\end{enumerate}
@author Milos Sramek
*/
