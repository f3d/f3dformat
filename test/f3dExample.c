/* f3dExample.c
 * This is an simple example showing how to use the {\tt f3dWriteCubGrid}
 * and {\tt f3dReadCubGrid} functions for different voxel types.
 *
 * This file is part of an f3d package, which was designed and partialy also
 * implemented during the author's post doctoral stay at SUNY an Stony Brook,
 * in the lab of Prof. Arie Kaufman.
 * 
 * For details, see the f3d World-Wide-Web page,
 * http://www.cs.sunysb.edu/~vislab/vxtools
 * or send a mail to milos.sramek@oeaw.ac.at
 * 
 * Copyright (C) 1999 Free Software Foundation
 * Milos Sramek  <milos.sramek@oeaw.ac.at>
 * 
 * f3d package is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * the f3d package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with the f3d package -- see the file COPYING.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <f3d.h>

#define size 30
#define BANDS 5

main()
{
	char	*fileName = "f3dExample.f3d";
	int	i;
	char	*comm[]={"Mono test grid","tag value"};
	int 	nx, ny, nz, ncomm2;
	int 	nBands2;
	f3dDataType 	dType2;
	char 	**comm2;
	static 	f3dInt16 val = 12345,		 
		    g1[BANDS*size*size*size],
		    *g2;

	for(i=0; i<BANDS; i++)
		g1[(2*i+1)*size*size*size / 2] = val;

	f3dWriteCubGrid(fileName,g1,size,size,size, 
					BANDS,f3dInt16Type,comm,2);

	f3dReadCubGrid(fileName,(void **)&g2,&nx, &ny, &nz, &nBands2, &dType2, 
						&comm2, &ncomm2);

	if(nBands2 != BANDS || dType2 != f3dInt16Type){
		fprintf(stderr,"Reading voxel type failed!!\n");
		exit(-1);
	}

	if(nx != size || ny != size || nz != size){
		fprintf(stderr,"Reading dimensions failed!!\n");
		exit(-1);
	}

	for(i=0; i<BANDS; i++)
	if (g1[(2*i+1)*size*size*size / 2] != g2[(2*i+1)*size*size*size / 2]){
		fprintf(stderr,"Reading data in band %d failed!!", i);
		exit(-1);
	}
	fprintf(stderr,"f3dExample: Test OK\n");
	return 0;
}



/**@name A simple C example
This is an simple example showing how to use the {\tt f3dWriteCubGrid}
and {\tt f3dReadCubGrid} functions for different voxel types.

{\bf File name:} \URL[f3d/src/f3dtest.c]{f3dExample.c}

@author Milos Sramek
*/
