/* f3dTFun.c
 * This is an simple example showing how to use store lookup tables in f3d files
 *
 * This file is part of an f3d package, which was designed and partialy also
 * implemented during the author's post doctoral stay at SUNY an Stony Brook,
 * in the lab of Prof. Arie Kaufman.
 * 
 * For details, see the f3d World-Wide-Web page,
 * http://www.cs.sunysb.edu/~vislab/vxtools
 * or send a mail to milos.sramek@oeaw.ac.at
 * 
 * Copyright (C) 1999 Free Software Foundation
 * Milos Sramek  <milos.sramek@oeaw.ac.at>
 * 
 * f3d package is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * the f3d package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with the f3d package -- see the file COPYING.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <f3d.h>

#define size 30
#define nbands1 2
#define ntf 2
#define dtype1 f3dUInt16Type

#define Calloc(x,y) (y *)calloc((unsigned)(x), sizeof(y))

main()
{
	char	*fileName = "f3dTFun.f3d";
	int	i, j, k;
	char	*comm[]={"LookupTable test grid","tag value"};
	static 	f3dInt16 val = 12345,		 
		    g1[nbands1*size*size*size],
		    *g2;
	f3dHeader hdr2, hdr1;
	char *tf[ntf*5] = {
		"Test transfer function 1",
		"0 0  50 0.1  100 0.5 255 0",
		"0 1.0  50 0.1  100 0.5 255 0",
		"0 0  50 0.1  100 0.5 255 1.0",
		"0 0  255 1.0",
		"Test transfer function 2",
		"0 0  75 0.1  100 0.5 255 0",
		"0 1.0  75 0.1  100 0.5 255 0",
		"0 0  75 0.1  100 0.5 255 1.0",
		"0 0  100 0.5 255 1.0",
	};
	

	hdr1 = f3dDefaultHeader(size, size, size, dtype1, nbands1);
	hdr1.tfnum = ntf;
	hdr1.tf = tf;

	for(i=0; i<nbands1; i++)
		g1[(2*i+1)*size*size*size / 2] = val;

	f3dWriteGrid(fileName, g1, &hdr1);

	f3dReadGrid(fileName, &g2, &hdr2);

	if(hdr2.dtype != hdr1.dtype){
		fprintf(stderr,"Reading voxel type failed!!\n");
		exit(-1);
	}

	if(hdr2.nx != hdr1.nx || hdr2.ny != hdr1.nx || hdr2.nz != hdr1.nx){
		fprintf(stderr,"Reading dimensions failed!!\n");
		exit(-1);
	}

	for(i=0; i<nbands1; i++)
	if (g1[(2*i+1)*size*size*size / 2] != g2[(2*i+1)*size*size*size / 2]){
		fprintf(stderr,"Reading data in band %d failed!!\n", i);
		exit(-1);
	}

	if(hdr1.tfnum != hdr2.tfnum){
		fprintf(stderr,"Reading TF number %d failed!!\n", hdr2.tfnum);
		exit(-1);
	}

	f3dPrintHeader(stderr, &hdr1);
	f3dPrintHeader(stderr, &hdr2);
	for(i=0; i<ntf*5; i++){
		if(strcmp(hdr1.tf[i], hdr2.tf[i])){
			fprintf(stderr,"TF %d comparison failed!!\n", i);
			exit(-1);
		}
	}
	fprintf(stderr,"f3dTFun: Test OK\n");
	return 0;
}



/**@name A simple C example
This is an simple example showing how to use the {\tt f3dWriteCubGrid}
and {\tt f3dReadCubGrid} functions for different voxel types.

{\bf File name:} \URL[f3d/src/f3dtest.c]{f3dTFun.c}

@author Milos Sramek
*/
