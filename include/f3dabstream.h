/* f3dbstream.h
 *
 * Prototypes of ``f3dbstract stream'' functions providing common interface to files and buffers.
 *
 * This file is part of an f3d package, which was designed and partialy also
 * implemented during the author's post doctoral stay at SUNY an Stony Brook,
 * in the lab of Prof. Arie Kaufman.
 *
 * For details, see the f3d World-Wide-Web page,
 * http://www.viskom.oeaw.ac.at/~milos/page/Download.html
 * or send a mail to milos.sramek@oeaw.ac.at
 *
 * Copyright (C) 1999 Free Software Foundation
 * Milos Sramek  <milos.sramek@oeaw.ac.at>
 *
 * f3d package is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * the f3d package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with the f3d package -- see the file COPYING.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* This file written by Viliam Solcany <viliam.solcany@oeaw.ac.at>, 2007.*/
/* Abstream (abstract stream) is a generalization of FILE* used in f3d. 
 * The purpose is to provide a common interface 
 * to reading from/writing to ordinary files and memory buffers.
 * The latter is useful for thread (process) communication in streamed processing
 */
#ifndef __ABSTREAM_H__
#define __ABSTREAM_H__

#include "f3dmsem.h"

typedef enum {W_NONE = 0, W_FILE = 1, W_USSB, W_THB} SType;
typedef struct {
	SType type;
	union {		/* they are mutually exclusive */
	FILE *fptr;	/* ordinary file */
	void *ussbptr;  /* IPC buffer shared by processes (based on SYS V shmget() call) */
	void *thbptr;   /* buffer for thread communication */
	};
} ABSTREAM; /* a generalization of FILE* */

#define ISFILE(ptr) (((ABSTREAM *)ptr)->type == W_FILE)

#ifdef __cplusplus
 extern "C" {
#endif

/*returns amount of data [bytes] that can be written to buffer without blocking */
/*ONLY for memory buffers */
size_t abfspace_w(FILE *stream);
/*returns amount of data [bytes] that can be read from buffer without blocking */
/*ONLY for memory buffers */
size_t abfspace_r(FILE *stream);

/*sets stdin and stdout to point to a correct ABSTREAM */
int abfstdd(const char *si, FILE **in_rplc, const char *so, FILE **out_rplc);


FILE *abfopen(const char *name, const char *mode);
int abfclose(FILE *stream);
size_t abfread(void *ptr, size_t size, size_t n, FILE *stream);
size_t abfwrite(const void *ptr, size_t size, size_t n, FILE *stream);
int abfflush(FILE *stream);


/* fgets() reads in at most one less than size characters from stream and
 * stores them into the buffer pointed to by  s. Reading  stops after  an  EOF
 * or  a newline. If a newline is read, it is stored into the buffer.  A ’\0’
 * is stored after the last character in the buffer.  fgets() returns s on
 * success, and NULL on error or when end of file occurs while no characters
 * have been read.
 */
char *abfgets(char *s, int size, FILE *stream);

int abfprintf(FILE *stream, const char *format, ...);

/* these functions are wrappers of thb_ functions that need to be callable directly from user code */
int thbw_create(const char *name, int block_sz, int num_blocks, int dbg);
int thbw_destroy(FILE *stream); //(void const *bptr);
int thbw_regal1empty(FILE* stream, msem_t *s1e); //(const void *bptr, msem_t *s1e);
int thbw_regal1full(FILE* stream, msem_t *s1f); //(const void *bptr, msem_t *s1f);

#ifdef __cplusplus
 }
#endif

#endif 
