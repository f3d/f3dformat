#ifndef __F3DFORMAT_DEFINES___
#define __F3DFORMAT_DEFINES___


// Generic helper definitions for shared library support
#if defined _WIN32 || defined __CYGWIN__
	#ifdef BUILD_f3dformat_SHARED
		#define F3DFORMAT_HELPER_DLL_IMPORT __declspec(dllimport)
		#define F3DFORMAT_HELPER_DLL_EXPORT __declspec(dllexport)
		#define F3DFORMAT_HELPER_DLL_LOCAL
	#else
		#define F3DFORMAT_HELPER_DLL_IMPORT extern
		#define F3DFORMAT_HELPER_DLL_EXPORT extern
		#define F3DFORMAT_HELPER_DLL_LOCAL
	#endif
#else
	#if __GNUC__ >= 4
		#define F3DFORMAT_HELPER_DLL_IMPORT __attribute__ ((visibility("default")))
		#define F3DFORMAT_HELPER_DLL_EXPORT __attribute__ ((visibility("default")))
		#define F3DFORMAT_HELPER_DLL_LOCAL  __attribute__ ((visibility("hidden")))
	#else
		#define F3DFORMAT_HELPER_DLL_IMPORT
		#define F3DFORMAT_HELPER_DLL_EXPORT
		#define F3DFORMAT_HELPER_DLL_LOCAL
	#endif
#endif

// Now we use the generic helper definitions above to define F3DFORMAT_API and
// F3DFORMAT_LOCAL. F3DFORMAT_API is used for the public API symbols. It either
// DLL imports or DLL exports. F3DFORMAT_LOCAL is used for non-api symbols.

#ifdef F3DFORMAT_BUILDING
// defined if we are building the f3dformat DLL (instead of using it)
	#define F3DFORMAT_API F3DFORMAT_HELPER_DLL_EXPORT
#else
	#define F3DFORMAT_API F3DFORMAT_HELPER_DLL_IMPORT
#endif

#define F3DFORMAT_LOCAL F3DFORMAT_HELPER_DLL_LOCAL

#endif

