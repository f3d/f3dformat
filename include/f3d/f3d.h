/*! \file f3d.h
\brief Constants, structures and prototypes for basic f3d file manipulation.

f3d (Format 3 Dimensional) is a simple format for storage of 3D datasets
supporting  different voxel types (8, 16, 32 bit integers, floats) and
different grids (cartesian, regular, rectilinear). It does not support
curvilinear and unstructured grids.  The package includes basic routines
for storage/retrieval of such data into/from a file.
*/

/*
Copyright (C) 1998-2003 by Milos Sramek

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
	claim that you wrote the original software. If you use this software
	in a product, an acknowledgment in the product documentation would be
	appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
	misrepresented as being the original software. In order to not to
	confuse the users, the 'f3d' name cannot be used in the case of such
	altered versions.
3. This notice may not be removed or altered from any source distribution.


Anyway, if you would like to see some new features, please, write to the
author with your recommendations and/or wishes.
*/

/*! \mainpage \c f3dformat package documentation
*
* \section intro Introduction
* f3d (Format 3 Dimensional) is a simple format for storage of 3D datasets
* supporting  different voxel types (8, 16, 32 bit signed/unsigned integers and
* 32 bit floats) and different grids (cartesian, regular, rectilinear). It does
* not support curvilinear and unstructured grids.  The package includes basic
* routines for storage/retrieval of such data into/from a file.
*
* \section ddoc Detailed reference
* \li \ref f3dheader
* \li \ref f3dvoxel
* \li \ref f3dread
* \li \ref f3dwrite
* \li \ref f3dother
*
* \section install Installation
*
* There are several possibilities, how to install the \c f3dformat package:
	\li From binaries by means of the \c rpm tool. The latest rpm-s for
		for i386 GNU/Linux systems can be found at
		http://www.viskom.oeaw.ac.at/~milos/Download
	\li From a source tarball by means of the `configure' shell script. This
		works well for the gnu systems. The tarball is available at
		http://www.viskom.oeaw.ac.at/~milos/Download
	\li From a source tarball by means of the provided Makefile.defs
		and Makefile.nognu files. For details see the Makefile.defs file.
*
* \section description Description
*
*
The \b f3d (<STRONG>F</STRONG>ormat <STRONG>3</STRONG>
<STRONG>D</STRONG>imensional) is aimed at storage and
manipulation of volumetric grids.
It was designed with the following aims on mind:

- <STRONG>Support of different kinds of voxels</STRONG>
Single and multiband data, 8, 16, 32 bit integers, signed and unsigned, floats
- <STRONG>Different kinds of grids</STRONG>
Cartesian, regular and rectilinear grids
\image html f3dgrids.png

- <STRONG>Self descriptive</STRONG> An \b f3d file contains all information
necessary for its processing. This information is to be accessible
with basic tools as, for example UNIX \c  head
command or a general image or text file viewers (\c xv, {\c vi}).
- <STRONG>Application specific comments</STRONG>  provide a means to transfer information
between
different applications (for example, from voxelization to visualization).
- <STRONG>Data compression</STRONG> Volumetric data are usually huge, but they often
compress very well (especially segmented masks or voxelized data).
- <STRONG>Preview image</STRONG> An icon image, giving a notion about the contents of the
file. Should be viewable by regular image viewers ({\tt xv}).


\subsection f3dstruct Basic structure of an f3d file

The \b f3d format  is an extension of the well known Portable Graymap
(\c pgm) format, defined in the \c pbmplus and \c netpbm packages,
which make it viewable (at least a part of it - an icon image) by standard
image viewers.
\b f3d extends \c pgm in several directions:

<STRONG>f3d specific comments</STRONG>

The \c pgm format enables to include arbitrary text comments in the
header part of the file.
\b f3d takes advantage of this feature by specifying \b f3d comments
with the following structure:

<tt> \#!keyword value</tt>

The keywords have their counterparts in members of the \c f3dHeader structure.
Detailed description can be found directly in the source code of
the \c f3dReadHeader and \c f3dWriteHeader functions.
The \c commLines keyword specifies number of application dependent
comments, which can be used to transfer information between different
application and do not have any specific format.
These comments are labeled by the \c comment keyword.
Of course, \c commLines must precede any \c comment line.

<STRONG>f3d file header example:</STRONG>
A single band cubic grid of <tt> unsigned char</tt> type voxels.

<tt>#P5					</tt> - pgm format (magic number)<br>
<tt>#128 128				</tt> - pgm format (image dimensions - in this case the icon image)<br>
<tt> \#!f3d   	5.13  			</tt> - f3d parameter<br>
<tt> \#******************************************************* </tt><br>
<tt> \#**                      f3d                            </tt><br>
<tt> \#**   Format for storage of 3D volumetric data sets     </tt><br>
<tt> \#**                                                     </tt><br>
<tt> \#**        http://www.cs.sunysb.edu/~milos/f3d          </tt><br>
<tt> \#******************************************************* </tt><br>
<tt> \#!endian 	f3dBigEndian		</tt> - f3d parameter <br>
<tt> \#!vdim  	128 128 128		</tt> - f3d parameter <br>
<tt> \#!vtype 	f3dCubic		</tt> - f3d parameter <br>
<tt> \#!px	1.000000		</tt> - f3d parameter <br>
<tt> \#!bands 	1			</tt> - f3d parameter <br>
<tt> \#!dtype 	f3dUChar		</tt> - f3d parameter <br>
<tt> \#!units 	f3dUmm			</tt> - f3d parameter <br>
<tt> \#!comment vxt thresh 0.5		</tt> - f3d comment <br>
<tt> \#!comment vxt profile linear 1.8	</tt> - f3d comment <br>
<tt> \#!comment File created by 'vxtGrid3D'	</tt> - f3d comment <br>
<tt> \#!last				</tt> - f3d parameter <br>
<tt> 255				</tt> - pgm format  <br>


<STRONG>f3d icon image</STRONG>

The purpose of the icon image is to provide the user with a notion
of what data  is in the file.
If \c f3dWriteGrid or \c f3dWriteCubGrid functions are used for
storing the data, a standard icon image is created by summation
of density values along X, Y or Z grid axis. This direction is determined
by the \c iconDir} member of the f3dHeader structure and can have
\c f3dIconX, \c f3dIconY and \c f3dIconZ values respectively.

<STRONG>Appended volumetric data</STRONG>

Volumetric data are appended directly after the \c pgm image data.
The data is stored slice by slice, with the following fields for each
slice:
-# length of the compressed slice. It is stored as a 32 bit unsigned integer
with MSB first order.
-# the compressed slice data. The \c compress} / \c uncompress} pair
of routines from the zlib(http://www.cdrom.com/pub/infozip/zlib/)
package is used.
multiband volumes are stored as consecutive monochromatic layers in
each with the length a compressed data fields.

<p>
\b f3d format and tools have been developed in a GNU/Linux environment.
However, they compile also under the Borland and Microsoft compilers in the
Windows environment.
*/


#ifndef F3D_H
#define F3D_H

#include "f3dFormatDefines.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <f3d/f3dHeader.h>

//#define sscanf	sscanf_s
//#define strdup	_strdup

/* prototypes */
#ifdef __cplusplus
extern "C"
{
#endif

/** type out the error message
* if without parameter, the stored error code is used
*/
F3DFORMAT_API const char * f3dErrorMsg(f3dError code);

#if defined(__STDC__) || defined(__cplusplus) || defined(__BORLANDC__) || defined(_MSC_VER)
/* use the prototypes for ANSI C and C++ */

/** \defgroup f3dread Reading f3d files
* A set of basic functions for reading \c f3d files
*/

/**@name C functions for reading f3d files
\ingroup f3dread
*/

/*@{*/
/**
\brief Read volumetric data from an \b f3d file.

@param name name of the file
@param buf Reference to a pointer to the 3D grid. A new grid is allocated and
filled with the volume data.
Proper handling of the voxel and color type is necessary in further processing.
Access to the <tt>[x,y,z]</tt> voxel:

<tt>*(buf + z*(hdr.ny*hdr.nx) + y*hdr.nx + x)</tt>
@param hdr f3d header structure with data desription
@param slice size, in voxels
@return 0 on success, non-zero number on failure
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API int f3dReadGrid(char *name, void **buf, f3dHeader *hdr);

/**
	\brief Read cubic grid from a file in f3d format.
	Simplified version of f3dReadGrid.

@param name name of the file
@param buf Reference. Pointer to a new allocated grid with the volume data.
Proper handling of the voxel and color type is necessary.
Access to the <tt>[x,y,z]</tt> voxel:

<tt>*(buf + z*(hdr.ny*hdr.nx) + y*hdr.nx + x)</tt>
@param nx Reference. x grid dimension
@param ny Reference. y grid dimension
@param nz Reference. z grid dimension
@param nbands Reference. Number of bands
@param dtype Reference. Data type
@param txt Reference. Array of string comments
@param ntxt Reference. Number of coment lines
@return 0 on success, non-zero number on failure
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API int f3dReadCubGrid(char *name, void **buf, int *nx, int *ny,
	int *nz, int *nbands, f3dDataType *dtype, char **txt[], int *ntxt);

/**
\brief Read header of an \b f3d file.

@param name name of the file
@param hdr Reference. f3dHeader structure with detailed information
about the grid.
@return if succesfull, pointer to the open file.
File pointer is set to the begining
of the data section, which can be read slice by slice by the \c f3dReadSlice
function.
If not successful, returns NULL and the \c f3dErrorCode variable is set.
<tt>f3rErrorMsg()</tt> then defines an error message. For example:
\verbatim
...
#include <f3d.h>

...
fptr = f3dReadHeader(name, &hdr);
if(fptr == NULL){
	fprintf(stderr,"%s\n", f3dErrorMsg());
	exit(1);
}
\endverbatim
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API FILE *f3dReadHeader(char *name, f3dHeader *hdr);

/**
	\brief Read header from an open \b f3d file.

	@param fptr FILE * pointer to an open file
	@param hdr Reference. f3dHeader structure with detailed information
	about the grid.
	@return if succesfull, pointer to the open file.
	File pointer is set to the begining
	of the data section, which can be read slice by slice by the f3dReadSlice
	function.
	If not successful, returns NULL and the \c f3dErrorCode variable is set.
	<tt>f3rErrorMsg()</tt> then defines an error message. For example:
	\verbatim
	...
	#include <f3d.h>

	fptr = stdin;
	...
	fptr = f3dReadHeaderF(fptr, &hdr);
	if(fptr == NULL){
		fprintf(stderr,"%s\n", f3dErrorMsg());
		exit(1);
	}
\endverbatim
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API FILE *f3dReadHeaderF(FILE *fptr, f3dHeader *hdr);


/**
\brief Read header and icon image of an \b f3d file.

@param fptr FILE * pointer to an open file
@param hdr Reference. f3dHeader structure with detailed information
about the grid.
@param img allocated array, storing the preview icon image. Should be freed by
free().
@param nx returns row length of the icon
@param ny returns number of rows of the icon
@param color returns true, if icon is color (rgb byte irder)
@return if succesfull, pointer to the open file.
File pointer is set to the begining
of the data section, which can be read slice by slice by the f3dReadSlice
function.
If not successful, returns NULL and the \c f3dErrorCode variable is set.
<tt>f3rErrorMsg()</tt> then defines an error message. For example:
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/

F3DFORMAT_API FILE *f3dReadHeaderAndIconF(FILE *fptr, f3dHeader *hdr,
	unsigned char **img, int *nx, int *ny, int *color);

/**
\brief Read header and icon image of an \b f3d file.

@param name name of the file
@param hdr Reference. f3dHeader structure with detailed information
about the grid.
@param img allocated array, storing the preview icon image. Should be freed by
free().
@param nx returns row length of the icon
@param ny returns number of rows of the icon
@param color returns true, if icon is color (rgb byte irder)
@return if succesfull, pointer to the open file.
File pointer is set to the begining
of the data section, which can be read slice by slice by the f3dReadSlice
function.
If not successful, returns NULL and the \c f3dErrorCode variable is set.
<tt>f3rErrorMsg()</tt> then defines an error message. For example:
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/

F3DFORMAT_API FILE *f3dReadHeaderAndIcon(char *name, f3dHeader *hdr,
	unsigned char **img, int *nx, int *ny, int *color);

/**
\brief Read one data slice of an \b f3d file.

@param fp Pointer to an open \b f3d file, as returned by f3dReadHeader.
@param buf Pointer to an allocated array with a proper size, to store the data.
@param etype endianness of the data
@param voxel size, in bytes
@param slice size, in voxels
@param ctype compression type
@return 0 on success, non-zero number on failure
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API int f3dReadSlice(FILE *fp, void *buf, f3dEndianType etype,
	int voxelSize, int sliceSize, f3dCompType ctype);
/* changed since verion 6.0
void f3dReadSlice(FILE *fp, void *buf, f3dEndianType etype, int voxelSize, int sliceSize);
*/
/*
* changed since verion 5.4
void f3dReadSlice(FILE *fp, void *buf, const f3dHeader *hdr);
*/
/*@}*/

/** \defgroup f3dwrite Writing f3d files
* A set of basic functions for writing \c f3d files
*/

/**
\brief Skip one data slice of an \b f3d file.

@param fp Pointer to an open \b f3d file, as returned by f3dReadHeader.
@param buf Pointer to an allocated array with a proper size, to store the data.
@param voxel size, in bytes
@param slice size, in voxels
@return 0 on success, non-zero number on failure
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API int f3dSkipSlice(FILE *fptr);

/**@name C functions for writing f3d files
\ingroup f3dwrite
*/
/*@{*/

/**
\brief Write volumetric data to \b f3d file.

@param name name of the file
@param buf Pointer to the grid with the volume data.
@param hdr f3d header structure with detailed information about
the grid.
@return 0 on success, non-zero number on failure
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API int f3dWriteGrid(char *name, const void *buf, f3dHeader *hdr);

/**
\brief Write volumetric data to an open \b f3d file.

@param fp FILE * to an open file
@param buf Pointer to the grid with the volume data.
@param hdr f3d header structure with detailed information about
the grid.< @return nothing
@return 0 on success, non-zero number on failure
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API int f3dWriteGridF(FILE *fp, const void *buf, f3dHeader *hdr);


/**
	\brief Write cubic grid to a file in f3d format.
	Simplified version of f3dWriteGrid.

@param name name of the file
@param buf Pointer to the grid with the volume data.
@param nx x grid dimension
@param ny y grid dimension
@param nz z grid dimension
@param nbands number of bands
@param dtype data type
@param txt Array of string comments
@param ntxt Number of coment lines
@return 0 on success, non-zero number on failure
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API int f3dWriteCubGrid(char *name, void *buf, int nx, int ny, int nz,
	int nbands, f3dDataType dtype, char *txt[], int ntxt);

/**
	\brief Write cubic grid to an open file in f3d format.
	Simplified version of f3dWriteGrid.

@param *fptr open file to write the data in
@param buf Pointer to the grid with the volume data.
@param nx x grid dimension
@param ny y grid dimension
@param nz z grid dimension
@param nbands Number of bands
@param dtype data type
@param txt Array of string comments
@param ntxt Number of coment lines
@return 0 on success, non-zero number on failure
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API int f3dWriteCubGridF(FILE *fptr, void *buf, int nx, int ny,
	int nz, int nbands, f3dDataType dtype, char *txt[], int ntxt);

/**
\brief Print the f3dHeader to a file. To be used mainly for testing.

@param fptr A file open for writing
@param hdr f3dHeader structure with detailed information about the grid.
@return 0 on success, non-zero number on failure
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API int f3dPrintHeader(FILE *fptr, const f3dHeader *hdr);

/**
\brief Write the f3dHeader to a  file.

@param fptr A file open for writing
@param hdr f3dHeader structure with detailed information about the grid.
@param img <tt>hx x hy</tt> image to be stored as the preview icon.
<tt>img</tt> is a pointer to a linear array of <tt>unsigned char</tt> pixels.
@param hx x dimension of preview icon <tt>img</tt>.
@param hy y dimension of preview icon <tt>img</tt>.
@return 0 on success, non-zero number on failure
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API int f3dWriteHeader(FILE *fptr, const f3dHeader *hdr,
	const unsigned char *img, int hx, int hy)  ;

/**
\brief Write one data slice to the \b f3d file.

@param fptr Pointer to an open file.
@param buf Pointer to the volume array.
@param voxel size, in bytes
@param slice size, in voxels
@return 0 on success, non-zero number on failure
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API int f3dWriteSlice(FILE *fptr, const void *buf, int voxelSize,
	int sliceSize, f3dCompType ctype);
/* changed since version 5.4
void f3dWriteSlice(FILE *fptr, const void *buf, int voxelSize, int sliceSize);
*/
/* changed since version 5.4
void f3dWriteSlice(FILE *fptr, const void *buf, const f3dHeader *hdr);
*/
/*@}*/

/** \defgroup f3dother Miscelanous functions
* A set of miscelanous functions for manipulation of f3d data
*/

/**@name Other C functions for manipulation with f3d volumes
\ingroup f3dother
*/
/*@{*/

/**
\brief Get minimum possible value of the given type.

Returns 0.0 for float
*/
F3DFORMAT_API float f3dMinT(f3dDataType dtype);

/**
\brief Get maximum possible value of the given type.

Returns 1.0 for float
*/
F3DFORMAT_API float f3dMaxT(f3dDataType dtype);

/**
\brief Compute voxel size from data type

@param dtype voxel type
@return on success voxel size in bytes, negative number on failure
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API int f3dVoxelSizeFromType(f3dDataType dtype);

/**
\brief Compute voxel size from information in f3dHeader structure.

@param hdr f3dHeader structure with detailed information about the grid.
@return on success voxel size in bytes, negative number on failure
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API int f3dVoxelSize(const f3dHeader *hdr);

/**
\brief Determine endian type of the actual computer.

@return <tt>f3dBigEndian</tt> or <tt>f3dLittleEndian</tt>
@return 0 on success, non-zero number on failure
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API int f3dHostType(void);

/**
\brief Destroy an f3dHeader variable.

If necessary, deallocates the comments and voxel dimension arrays.
@param hdr f3dHeader structure with detailed information about the grid.
@return 0 on success, non-zero number on failure
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API int f3dDelHeader(f3dHeader *hdr);

/**
\brief Duplicate an f3dHeader variable.

Copies all static members and duplicates all arrays.
@param hdr f3dHeader structure with detailed information about the grid.
@return Duplicated header or new header with zero dimensions in case hdr was
	NULL.
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API f3dHeader f3dDupHeader(f3dHeader const *hdr);

/**
\brief Create an f3dHeader structure with default settings.

@param nx x grid dimension
@param ny y grid dimension
@param nz z grid dimension
@param dtype voxel type
@param nbands Number of bands
@return New header
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API f3dHeader f3dDefaultHeader(int nx, int ny, int nz,
	f3dDataType dtype, int nbands);

/**
\brief Destroy comments in an f3dHeader variable.

If necessary, deallocates the comments.
@param hdr f3dHeader structure with detailed information about the grid.
@return 0 on success, non-zero number on failure
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API int f3dDelHdrComment(f3dHeader *hdr);

/**
\brief Sets comments in an f3dHeader variable.

If necessary, deallocates the old comments, allocates space forthe new ones
and copies them.
@param hdr f3dHeader structure with detailed information about the grid.
@param n Number of new comment lines
@param txt Array of new commens
@return 0 on success, non-zero number on failure
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API int f3dSetHdrComment(f3dHeader *hdr, int n, char **txt);

/**
\brief Add a comment line to an f3dHeader variable.

@param hdr f3dHeader structure with detailed information about the grid.
@param txt The new commens
@return 0 on success, non-zero number on failure
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API int f3dAddHdrComment(f3dHeader *hdr, const char *txt);

/**
\brief Sets Transfer Function in an f3dHeader variable.

If necessary, deallocates the old Transfer Functions, allocates space
for the new ones and copies them.
@param hdr f3dHeader structure with detailed information about the grid.
@param n Number of new TF lines
@param txt Array of new TFs
@return 0 on success, non-zero number on failure
@author Dusan Migra
int f3dSetTF(f3dHeader *hdr, int n, char **txt);
*/

/**
\brief Add a Transfer Function to an f3dHeader variable.

@param hdr f3dHeader structure with detailed information about the grid.
@param txt the new TF
@return 0 on success, non-zero number on failure
@author Dusan Migra
*/
F3DFORMAT_API int f3dAddHdrTF(f3dHeader *hdr, const char **txt);

/**
\brief Delete a Transfer Function in an f3dHeader variable.

Deletes the required TF.
@param hdr f3dHeader structure with detailed information about the grid.
@param int which number of the TF to delete
@return 0 on success, non-zero number on failure
@author Dusan Migra
*/
F3DFORMAT_API int f3dDelHdrTF(f3dHeader *hdr, int which);

/**
\brief Render the default icon image by summing voxel densities.

Either monochromatic or color (RGB) data are created, depending on the
number of data bands

Direction projection is along the Z axis
@param buf 3D volume data grid
@param hdr f3dHeader structure with detailed information about the grid.
@param iw Reference. Returns width of the icon image.
@param ih Reference. Returns height of the icon image.
@return Pointer to <tt>iw x ih</tt> image of <tt>unsigned char</tt> pixels or
	NULL in case of failure.
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
F3DFORMAT_API unsigned char *f3dCreateIconImage(const void *buf,
	const f3dHeader *hdr, int *iw, int *ih);

/**
\brief Obtain version of the f3dformat library

Version of the f3dformat library consist of 2 values - major and minor version.
These values are the same as version of the f3d file. F3d file format is
the same across different minor versions meaning only changes in the
implementation of the library. Change in the major version makes library
backwards incompatible.

@param[out] major Major library version
@param[out] minor Minor library version
@return 0 on success (always succeeds)
@author Michal Hucko
*/
F3DFORMAT_API int f3dLibraryVersion(int* major, int* minor);

/*@}*/

#ifdef __cplusplus
}
#endif

#else /*defined(__STDC__) || defined(__cplusplus)*/
/* K&R C headers */


F3DFORMAT_API const char* f3dErrorMsg();
F3DFORMAT_API const char* f3dErrorMsg();
F3DFORMAT_API int f3dReadGrid();
F3DFORMAT_API int f3dReadCubGrid();
F3DFORMAT_API FILE *f3dReadHeader();
F3DFORMAT_API FILE *f3dReadHeaderAndIcon();
F3DFORMAT_API FILE *f3dReadHeaderF();
F3DFORMAT_API FILE *f3dReadHeaderAndIconF();
F3DFORMAT_API int f3dReadSlice();
F3DFORMAT_API int f3dWriteGrid();
F3DFORMAT_API int f3dWriteCubGrid();
F3DFORMAT_API int f3dPrintHeader();
F3DFORMAT_API int f3dWriteHeader();
F3DFORMAT_API int f3dWriteSlice();
F3DFORMAT_API int f3dVoxelSize();
F3DFORMAT_API int f3dVoxelSizeFromType();
F3DFORMAT_API int f3dHostType();
F3DFORMAT_API int f3dDelHeader();
F3DFORMAT_API f3dHeader f3dDupHeader();
F3DFORMAT_API int f3dDelHdrComment();
F3DFORMAT_API int f3dSetHdrComment();
F3DFORMAT_API int f3dAddHdrComment();
F3DFORMAT_API int f3dSetTF();
F3DFORMAT_API int f3dAddHdrTF();
F3DFORMAT_API int f3dDelHdrTF();
F3DFORMAT_API unsigned char *f3dCreateIconImage();
F3DFORMAT_API float f3dMinT();
F3DFORMAT_API float f3dMaxT();
#endif  /*defined(__STDC__) || defined(__cplusplus)*/

#endif /* F3D_H */
