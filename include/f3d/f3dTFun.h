/*! \file f3dTFun.h
 \brief Casic C-code for manipulation with transfer functions

f3d (Format 3 Dimensional) is a simple format for storage of 3D datasets
supporting  different voxel types (8, 16, 32 bit integers, floats) and
different grids (cartesian, regular, rectilinear). It does not support
curvilinear and unstructured grids.  The package includes basic routines
for storage/retrieval of such data into/from a file.
*/

/*
 Copyright (C) 1998-2003 by Milos Sramek

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software. In order to not to
     confuse the users, the 'f3d' name cannot be used in the case of such
     altered versions.
  3. This notice may not be removed or altered from any source distribution.


  Anyway, if you would like to see some new features, please, write to the
  author with your recommendations and/or wishes.
*/

#ifndef F3DTFUN_H
#define F3DTFUN_H

#include "f3dFormatDefines.h"

/** A transfer function control point
Components:
x -- x coordinate of the control point
v -- value from <0,1>
*/
typedef struct _f3dTFPoint{
	float x;
	float v;
} f3dTFPoint;

/** A transfer function defined by tflen control points
Components:
len -- number of control points
p -- array of control points
*/
typedef struct _f3dTFun {
	int len;
	f3dTFPoint *p;
} f3dTFun;

/**
RGBA transfer function
*/
typedef struct _f3dTFRGBA {
	f3dTFun r;
	f3dTFun g;
	f3dTFun b;
	f3dTFun a;
} f3dTFRGBA;

/* prototypes */
#ifdef __cplusplus
extern "C"
{
#endif

/* Create and empty RGBA transfer function */
F3DFORMAT_API f3dTFun *f3dNewTFRGBA(void);

/** add a point to a transfer function. Points are sorted according to the x coordinate.
@param tf the transfer function
@param p pointer to the point to be added
*/
F3DFORMAT_API int f3dAddTFunPoint(f3dTFun *tf, f3dTFPoint *p);

/** delete a point from a transfer function
@param pos the point to delete
@param the transfer function
*/
F3DFORMAT_API int f3dDelTFunPoint(f3dTFun *tf, int pos);

/** delete the transfer function structure */
F3DFORMAT_API int f3dFreeTFun(f3dTFun *tf);

#ifdef __cplusplus
}
#endif

#endif  /*F3DTFUN_H*/
