/*! \file f3dHeader.h
\brief Constants, structures and prototypes for basic f3d file manipulation.

f3d (Format 3 Dimensional) is a simple format for storage of 3D datasets
supporting  different voxel types (8, 16, 32 bit integers, floats) and
different grids (cartesian, regular, rectilinear). It does not support
curvilinear and unstructured grids.  The package includes basic routines
for storage/retrieval of such data into/from a file.
*/

/*
Copyright (C) 1998-2003 by Milos Sramek

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
	claim that you wrote the original software. If you use this software
	in a product, an acknowledgment in the product documentation would be
	appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
	misrepresented as being the original software. In order to not to
	confuse the users, the 'f3d' name cannot be used in the case of such
	altered versions.
3. This notice may not be removed or altered from any source distribution.


Anyway, if you would like to see some new features, please, write to the
author with your recommendations and/or wishes.
*/

/*! \mainpage \c f3dformat package documentation
*
* \section intro Introduction
* f3d (Format 3 Dimensional) is a simple format for storage of 3D datasets
* supporting  different voxel types (8, 16, 32 bit signed/unsigned integers and
* 32 bit floats) and different grids (cartesian, regular, rectilinear). It does
* not support curvilinear and unstructured grids.  The package includes basic
* routines for storage/retrieval of such data into/from a file.
*
* \section ddoc Detailed reference
* \li \ref f3dheader
* \li \ref f3dvoxel
* \li \ref f3dread
* \li \ref f3dwrite
* \li \ref f3dother
*
* \section install Installation
*
* There are several possibilities, how to install the \c f3dformat package:
	\li From binaries by means of the \c rpm tool. The latest rpm-s for
		for i386 GNU/Linux systems can be found at
		http://www.viskom.oeaw.ac.at/~milos/Download
	\li From a source tarball by means of the `configure' shell script. This
		works well for the gnu systems. The tarball is available at
		http://www.viskom.oeaw.ac.at/~milos/Download
	\li From a source tarball by means of the provided Makefile.defs
		and Makefile.nognu files. For details see the Makefile.defs file.
*
* \section description Description
*
*
The \b f3d (<STRONG>F</STRONG>ormat <STRONG>3</STRONG>
<STRONG>D</STRONG>imensional) is aimed at storage and
manipulation of volumetric grids.
It was designed with the following aims on mind:

- <STRONG>Support of different kinds of voxels</STRONG>
Single and multiband data, 8, 16, 32 bit integers, signed and unsigned, floats
- <STRONG>Different kinds of grids</STRONG>
Cartesian, regular and rectilinear grids
\image html f3dgrids.png

- <STRONG>Self descriptive</STRONG> An \b f3d file contains all information
necessary for its processing. This information is to be accessible
with basic tools as, for example UNIX \c  head
command or a general image or text file viewers (\c xv, {\c vi}).
- <STRONG>Application specific comments</STRONG>  provide a means to transfer information
between
different applications (for example, from voxelization to visualization).
- <STRONG>Data compression</STRONG> Volumetric data are usually huge, but they often
compress very well (especially segmented masks or voxelized data).
- <STRONG>Preview image</STRONG> An icon image, giving a notion about the contents of the
file. Should be viewable by regular image viewers ({\tt xv}).


\subsection f3dstruct Basic structure of an f3d file

The \b f3d format  is an extension of the well known Portable Graymap
(\c pgm) format, defined in the \c pbmplus and \c netpbm packages,
which make it viewable (at least a part of it - an icon image) by standard
image viewers.
\b f3d extends \c pgm in several directions:

<STRONG>f3d specific comments</STRONG>

The \c pgm format enables to include arbitrary text comments in the
header part of the file.
\b f3d takes advantage of this feature by specifying \b f3d comments
with the following structure:

<tt> \#!keyword value</tt>

The keywords have their counterparts in members of the \c f3dHeader structure.
Detailed description can be found directly in the source code of
the \c f3dReadHeader and \c f3dWriteHeader functions.
The \c commLines keyword specifies number of application dependent
comments, which can be used to transfer information between different
application and do not have any specific format.
These comments are labeled by the \c comment keyword.
Of course, \c commLines must precede any \c comment line.

<STRONG>f3d file header example:</STRONG>
A single band cubic grid of <tt> unsigned char</tt> type voxels.

<tt>#P5					</tt> - pgm format (magic number)<br>
<tt>#128 128				</tt> - pgm format (image dimensions - in this case the icon image)<br>
<tt> \#!f3d   	5.13  			</tt> - f3d parameter<br>
<tt> \#******************************************************* </tt><br>
<tt> \#**                      f3d                            </tt><br>
<tt> \#**   Format for storage of 3D volumetric data sets     </tt><br>
<tt> \#**                                                     </tt><br>
<tt> \#**        http://www.cs.sunysb.edu/~milos/f3d          </tt><br>
<tt> \#******************************************************* </tt><br>
<tt> \#!endian 	f3dBigEndian		</tt> - f3d parameter <br>
<tt> \#!vdim  	128 128 128		</tt> - f3d parameter <br>
<tt> \#!vtype 	f3dCubic		</tt> - f3d parameter <br>
<tt> \#!px	1.000000		</tt> - f3d parameter <br>
<tt> \#!bands 	1			</tt> - f3d parameter <br>
<tt> \#!dtype 	f3dUChar		</tt> - f3d parameter <br>
<tt> \#!units 	f3dUmm			</tt> - f3d parameter <br>
<tt> \#!comment vxt thresh 0.5		</tt> - f3d comment <br>
<tt> \#!comment vxt profile linear 1.8	</tt> - f3d comment <br>
<tt> \#!comment File created by 'vxtGrid3D'	</tt> - f3d comment <br>
<tt> \#!last				</tt> - f3d parameter <br>
<tt> 255				</tt> - pgm format  <br>


<STRONG>f3d icon image</STRONG>

The purpose of the icon image is to provide the user with a notion
of what data  is in the file.
If \c f3dWriteGrid or \c f3dWriteCubGrid functions are used for
storing the data, a standard icon image is created by summation
of density values along X, Y or Z grid axis. This direction is determined
by the \c iconDir} member of the f3dHeader structure and can have
\c f3dIconX, \c f3dIconY and \c f3dIconZ values respectively.

<STRONG>Appended volumetric data</STRONG>

Volumetric data are appended directly after the \c pgm image data.
The data is stored slice by slice, with the following fields for each
slice:
-# length of the compressed slice. It is stored as a 32 bit unsigned integer
with MSB first order.
-# the compressed slice data. The \c compress} / \c uncompress} pair
of routines from the zlib(http://www.cdrom.com/pub/infozip/zlib/)
package is used.
multiband volumes are stored as consecutive monochromatic layers in
each with the length a compressed data fields.

<p>
\b f3d format and tools have been developed in a GNU/Linux environment.
However, they compile also under the Borland and Microsoft compilers in the
Windows environment.
*/


#ifndef F3DHEADER_H
#define F3DHEADER_H

/** Error codes returned by means of the \c f3dErrorCode variable and f3dErrorMsg function */
typedef enum {
	F3D_SHOW_ERROR = -1, /* no error code */
	F3D_NO_ERROR = 0,
	F3D_FILE_OPEN,
	F3D_PNM_HEADER_ERROR,
	F3D_INCORR_PNM_TYPE,
	F3D_ICON_IMAGE_ERROR,
	F3D_NOT_F3D_FORMAT,
	F3D_READ_HDR_LINE_ERROR,
	F3D_INCORR_ENDIAN_VAL,
	F3D_INCORR_VTYPE_VAL,
	F3D_INCORR_CTYPE_VAL,
	F3D_INCORR_NBANDS_VAL,
	F3D_INCORR_DTYPE_VAL,
	F3D_INCORR_UNITS_VAL,
	F3D_COMM_ALLOC_ERR,
	F3D_ALLOC_ERR,
	F3D_END_MISS,
	F3D_INCORR_DIM,
	F3D_VTYPE_MISS,
	F3D_UNITS_MISS,
	F3D_NBANDS_MISS,
	F3D_DTYPE_MISS,
	F3D_COMM_MISS,
	F3D_PX_MISS,
	F3D_PY_MISS,
	F3D_PZ_MISS,
	F3D_RX_MISS,
	F3D_RY_MISS,
	F3D_RZ_MISS,
	F3D_RX_ZERO,
	F3D_RY_ZERO,
	F3D_RZ_ZERO,
	F3D_TF_MANY,
	F3D_TF_FEW,
	F3D_TF_ALLOC,
	F3D_TFNUM_MISS,
	F3D_NOT_CURRENT,
	F3D_COMP_MISS
} f3dError;


/** \defgroup f3dvoxel Voxel types
* Supported voxel types
*
* The <tt>f3dDataType dtype</tt> member of the f3dHeader structure or the
* <tt>f3dDataType dtype</tt> argument of many functions takes/returns the following values, corresponding to the voxel type:
\li f3dUCharType
\li f3dCharType
\li f3dUInt12Type
\li f3dUInt16Type
\li f3dIntHUType
\li f3dInt16Type
\li f3dUInt32Type
\li f3dInt32Type
\li f3dFloatType
\li f3dDoubleType
*/

/**@name Data voxel types
\ingroup f3dvoxel
\b f3d files can store both single- and multiband data
with the following voxel types,
We boldly assume here that interger variable sizes are the same on
both 32 and 64 bit systems for all compilers. Thus, we do not do any tests.
*/
/**@{*/
typedef unsigned char f3dUChar;
typedef char	f3dChar;
typedef unsigned short f3dUInt16;
typedef unsigned short f3dUInt12;
typedef short f3dInt16;
typedef short f3dIntHU;
typedef unsigned int f3dUInt32;
typedef int f3dInt32;
typedef float f3dFloat;
typedef double f3dDouble;
/**@}*/

/* system independent definition of min/max value bounds of specific types */
#define f3dUCharMax 	255
#define f3dUCharMin 	0
#define f3dCharMax 	127
#define f3dCharMin 	(-f3dCharMax-1)

#define f3dUInt12Max 	4095
#define f3dUInt12Min 	0

#define f3dUInt16Max 	65535
#define f3dUInt16Min 	0

#define f3dIntHUMax 	3095
#define f3dIntHUMin 	-1000

#define f3dInt16Max 	32767
#define f3dInt16Min 	(-f3dInt16Max-1)

#define f3dUInt32Max 	4294967295U
#define f3dUInt32Min 	0
#define f3dInt32Max 	2147483647
#define f3dInt32Min 	(-f3dInt32Max-1)

#define f3dFloatMax	1e35
#define f3dFloatMin	-1e35

#define f3dDoubleMax	1e35
#define f3dDoubleMin	-1e35

/* color type: number of values per pixel */
/* obsolete since f3d-3.0 */
/*
typedef enum {
	f3dScalarType = 1,
	f3dVectorType
}f3dVoxelType;
*/

/** Maximum lenght of a header line
	We check this when reading, but not when writing the header
*/
#define MAX_HEADER_LINE_LEN 2000

/** Number of rows, a transfer function occupies in the file*/
#define TF_HEADER_LINES 5

/*! \enum f3dDataType
* \brief Definition of voxel data type
*/
typedef enum {
/*! unsigned char */	f3dUCharType = 1,
/*! char */		f3dCharType,
/*! unsigned short */	f3dUInt12Type,
/*! unsigned short */	f3dUInt16Type,
/*! signed short */	f3dInt16Type,
/*! signed short */	f3dIntHUType,
/*! unsigned long */	f3dUInt32Type,
/*! signed long */	f3dInt32Type,
/*! float */		f3dFloatType,
/*! double */		f3dDoubleType
}f3dDataType;

/*! when nothing is known */
#define f3dUnknown		0

/*! \enum f3dGridType
* \brief Definition of grid spacing types
*/
typedef enum {
/*! cubic grid */	f3dCubic = 1,
/*! regular grid */	f3dRegular,
/*! rectilinear in Z direction */	f3dRectZ,
/*! rectilinear in all directions */	f3dRectXYZ,
/*! sequences of regular grids*/	f3dZBlock
}f3dGridType;

/*! \enum
* \brief Unit type
*/
typedef enum {
/* micrometers */	f3dUum = 1,
/* millimeters */	f3dUmm,
/* meters */		f3dUm
} f3dUnitType;

/*! \enum f3dEndianType
* \brief Little/big endian definitions .
*
* data are stored regardless of the endian type.
* When loaded, if the file type disagrees with the host type,
* data are converted to the host endian type
*/
typedef enum {
	f3dBigEndian = 1,
	f3dLittleEndian
} f3dEndianType;

/** Read a scalar density with type 'type'  from a byte array */
#define DENS_VAL(buf, shift, type) *((type*)buf+shift)

/*! \enum f3dCompType
* \brief File compression type
*
* Type of the used compression
* Added in version
*/
typedef enum {
	f3dCompKeep = -1,
	f3dCompZlib = 1,
	f3dCompNone
} f3dCompType;

struct _ZBlock{
	float blockpx;
	float blockpy;
	float blockpz;
	/** number of slices per block.  */
	int blocknz;
};
typedef struct _ZBlock ZBlock;

/** \defgroup f3dheader The f3dHeader structure
*/

/*! \struct f3dHeader
\ingroup f3dheader
\brief A detailed description of 3D volume data.

This description is stored together with an icon image and the volumetric
data itself into an \b f3d format file.
Since  there are some non-static members in this structure, functions
f3dDupHeader and f3dDelHeader should be used for copying an deleting
of f3dHeader variables.
@author Milos Sramek http://www.viskom.oeaw.ac.at/~milos
*/
struct f3dHeader{
	/** version major number*/
	int versMaj;

	/** version minor number*/
	int versMin;

	/** File compression type */
	f3dCompType comptype;

	/** File endian type.

		If necessary (if type of the computer
		is not the same as that of file), order of bytes is reversed
		during data loading. Thus, byte reordering can happen only
		when files are transferred between computers.
	*/
	f3dEndianType endian;

	/** \var nx
	\brief x dimension - row length.  */
	int nx;

	/** y dimension - number of rows per slice.  */
	int ny;

	/** z dimension - number of slices per volume.  */
	int nz;

	/** Defines grid spacing type.  */
	f3dGridType vtype;

	/** Voxel size along the x axis

			Used for \c f3dCubic, \c f3dRegular and \c f3dRectZ grid types.
	*/
	float px;


	/** Voxel size along the y axis.

			Used for \c f3dCubic, \c f3dRegular and \c f3dRectZ grid types.
	*/
	float py;

	/** Voxel size along the z axis.

			Used for \c f3dCubic and \c f3dRegular grid types.
	*/
	float pz;

	/** Array of voxel sizes along the x axis.

			Used for \c f3dRectXYZ grid type.
	*/
	float *rx;

	/** Array of voxel sizes along the y axis.

			Used for \c f3dRectXYZ grid type.
	*/
	float *ry;

	/** Array of voxel sizes along the z axis.

			Used for \c f3dRectZ and \c f3dRectXYZ grid types.
	*/
	float *rz;

	/** Array of blocks along the z axis.

			Used for f3dZBlock grid types.
	*/

	ZBlock *block;

	/*number of blocks*/

	int nblocks;

	/** Physical dimension of voxel sizes.  */
	f3dUnitType  units;

	/** Number of bands

	Defines number of image bands (data multiplicity)
	Introduced in \c f3d version 3.0, replaces the ctype tag
	*/
	int nbands;

	/** Voxel data type.  */
	f3dDataType dtype;

	/** Background threshold -- used for run-length compression */
	int   bgtUsed;
	float bgtFloat;		/* for floating point types */
	int bgtInt;		/* for inteder types */

	/** Number of lines of user defined comments.  */
	int  commentLines;

	/** User defined comments.

		Comments can be deleted or  set by the
		\a f3dDelHdrComment and \a f3dSetHdrComment functions.
	*/
	char **comment;  /* multiline comments */

	/** Number of transfer functions.
		if tfnum == 0, no transfer functions are defined
	*/
	int tfnum;

	/** A (tfnum x 5) long array of transfer functions in a string form.
		A single TF is defined by 5 strings, the first being its name/description,
		followed by definition of the rgba components.
		An r,g,b or a transfer function is defined on a single line by a sequence of pairs
		'density value' with blanks as separators. Continuation lines are not allowed.

		TFs are ordered in the number/(text)rgba order, i.e (for 2 TFs):
		tf0 description
		tf0 r
		tf0 g
		tf0 b
		tf0 a
		tf1 description
		tf1 r
		...
		tf1 a
	*/
	char **tf;
};
typedef struct f3dHeader f3dHeader;
#endif /* F3DHEADER_H */
