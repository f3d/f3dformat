/* msem.h
 *
 * Semaphores used in thb buffers.
 *
 * This file is part of an f3d package, which was designed and partialy also
 * implemented during the author's post doctoral stay at SUNY an Stony Brook,
 * in the lab of Prof. Arie Kaufman.
 *
 * For details, see the f3d World-Wide-Web page,
 * http://www.viskom.oeaw.ac.at/~milos/page/Download.html
 * or send a mail to milos.sramek@oeaw.ac.at
 *
 * Copyright (C) 1999 Free Software Foundation
 * Milos Sramek  <milos.sramek@oeaw.ac.at>
 *
 * f3d package is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * the f3d package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with the f3d package -- see the file COPYING.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

 /* This file written by Viliam Solcany <viliam.solcany@oeaw.ac.at>, 2007.*/

#ifndef __MSEM_H__
#define __MSEM_H__

#include <pthread.h>

/* semaphore type for thb */
typedef struct {
	pthread_mutex_t lock;
	pthread_cond_t cond;
	int count;
} msem_t;

#ifdef __cplusplus
 extern "C" {
#endif
/* ``public'' semaphore functions*/
int msem_init(msem_t *sem, int pshared, unsigned int value);  /* pshared is there for compatibility with posix sem, here ignored */
int msem_destroy(msem_t *sem);
int msem_wait_q(msem_t *sem);

#ifdef __cplusplus
 }
#endif
#endif 

