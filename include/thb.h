/* thb.h 
 *
 * Interface to thb buffers. 
 *
 * This file is part of an f3d package, which was designed and partialy also
 * implemented during the author's post doctoral stay at SUNY an Stony Brook,
 * in the lab of Prof. Arie Kaufman.
 *
 * For details, see the f3d World-Wide-Web page,
 * http://www.viskom.oeaw.ac.at/~milos/page/Download.html
 * or send a mail to milos.sramek@oeaw.ac.at
 *
 * Copyright (C) 1999 Free Software Foundation
 * Milos Sramek  <milos.sramek@oeaw.ac.at>
 *
 * f3d package is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * the f3d package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with the f3d package -- see the file COPYING.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* This file written by Viliam Solcany <viliam.solcany@oeaw.ac.at>, 2007.*/
/* Buffers for multithreaded streaming. 
 * Used to pass data between threads, similarly to pipes between processes */

#ifndef __THB_H__
#define __THB_H__

#include <stdarg.h>

#ifdef __cplusplus
 extern "C" {
#endif

/*function prototypes for thb */

void *thb_open(const char *name, const char *mode);	/* create thb if it doesn't exist */

int thb_close(const void *bptr); 

size_t thb_write(const void *dataptr, const size_t elemsize, const size_t nelem, void const *bptr); 

size_t thb_read(void const *dataptr, const size_t elemsize, const size_t nelem, const void *bptr); 

int thb_fflush(const void *bptr);

/*returns amount of data [bytes] that can be written to buffer without blocking */
size_t thb_space_w(const void *bptr);
/*returns amount of data [bytes] that can be read from buffer without blocking */
size_t thb_space_r(const void *bptr);

char *thb_fgets(char *s, int size, const void *bptr);
int thb_vfprintf(void const *bptr, const char *format, va_list vaptr);

#ifdef __cplusplus
 }
#endif

#endif /* __THB_H__ */
