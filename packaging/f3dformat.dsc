Format: 1.0
Source: f3dformat
Version: 6.2
Binary: libf3dformat6
Maintainer: Milos Sramek <milos.sramek@oeaw.ac.at>
Architecture: any
Build-Depends: debhelper (>= 4.1.16), cmake, doxygen, libzip-dev
Files:
 d57283ebb8157ae919762c58419353c8 133282 f3dformat_6.2.orig.tar.gz