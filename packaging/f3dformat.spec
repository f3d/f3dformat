#
# spec file for package f3dFormat
#

# norootforbuild

Name:          f3dformat
License:       GPL
Group:         System/Libraries
Version:       6.2
Release:       0
Summary:       Basic routines for reading and writing f3d files
Source:        %name-%version.tar.gz
URL:           http://gerlach.viskom.oeaw.ac.at/f3dtrac
BuildRequires: cmake zlib-devel doxygen gcc-c++
BuildRoot:     %{_tmppath}/%{name}-%{version}-build

%description
f3d (Format 3 Dimensional) is a simple format for storage of 3D datasets supporting different voxel types (8, 16, 32 bit signed/unsigned integers and 32 bit floats) and different grids (cartesian, regular, rectilinear). It does not support curvilinear and unstructured grids. The package includes basic routines for storage/retrieval of such data into/from a file.

%package       -n lib%{name}6
Summary:       Basic routines for reading and writing f3d files
Group:         System/Libraries

%description   -n lib%{name}6
f3d (Format 3 Dimensional) is a simple format for storage of 3D datasets supporting different voxel types (8, 16, 32 bit signed/unsigned integers and 32 bit floats) and different grids (cartesian, regular, rectilinear). It does not support curvilinear and unstructured grids. The package includes basic routines for storage/retrieval of such data into/from a file.

%package       -n f3dformat-tools
Summary:       Command line tools for working with f3d files
Group:         Productivity/Scientific/Other
Requires:      lib%{name}6 = %{version}

%description   -n f3dformat-tools
The f3dformat-tools package contains command-line tools for work with f3d format volume files.

%package       -n lib%{name}-devel
Summary:       Include Files and Libraries mandatory for Development
Group:         Development/Libraries/C and C++
Requires:      lib%{name}6 = %{version} zlib-devel

%description   -n lib%{name}-devel
The libf3dformat-devel package contains libraries and header files for developing applications that use f3d format.

%prep
%setup -q

%build
mkdir build
cd build
cmake -DBUILD_f3dformat_SHARED=ON ..
make %{?_smp_mflags}
cd ../doc
doxygen Doxyfile
cd ..

%install
cd build
cmake -DBUILD_f3dformat_SHARED=ON -DCMAKE_INSTALL_PREFIX=%{buildroot}%{_prefix} ..
make install
cd ..

%clean
rm -rf %{buildroot}

%post -n lib%{name}6 -p /sbin/ldconfig

%postun -n lib%{name}6 -p /sbin/ldconfig

%files -n lib%{name}6
%defattr(-,root,root,-)
%{_libdir}/lib%{name}.so.*

%files -n f3dformat-tools
%defattr(-,root,root,-)
%{_bindir}/*

%files -n lib%{name}-devel
%defattr(-,root,root,-)
%dir %{_includedir}/f3d
%{_includedir}/f3d/*.h
%{_libdir}/lib%{name}.so
%doc doc/html
%doc AUTHORS ChangeLog COPYING INSTALL NEWS README THANKS

%changelog
