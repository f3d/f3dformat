/* f3dTFun.c   
 * implementation of the transfer function code

 Copyright (C) 1998-1999 by Milos Sramek

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software. In order to not to
     confuse the users, the 'f3d' name cannot be used in the case of such
     altered versions. 
  3. This notice may not be removed or altered from any source distribution.


  Anyway, if you would like to see some new features, please, write to the
  author with your recommendations and/or wishes.
*/

#include<f3d.h>

#define MyRealloc(ptr,x,y) ((y *)realloc((ptr),(unsigned)(x) * sizeof(y)))
#define MyCalloc(x,y) (y *)calloc((unsigned)(x), sizeof(y))

static int TFPointComp(const void *a, const void *b) 
{
	return ((f3dTFPoint *)a)->x > ((f3dTFPoint *)b)->x;
}

/** allocate a default transfer function defined by min and max densities
 */
f3dTFun* f3dDefaultTFun(float xmin, float xmax)
{
	f3dTFun *tf;

	if(!(tf = MyCalloc(1, f3dTFun))){
		fprintf(stderr,"f3dAllocateTFun: Allocation Error 1\n");
		return NULL;
	}

	tf->len = 2;

	f3dTFPoint aux;
	aux.x = xmin;
	aux.r = aux.g = aux.b = 0;
	aux.a = 0;
	f3dAddTFunPoint(tf, &aux);

	aux.x = xmax;
	aux.r = aux.g = aux.b = 1;
	aux.a = 1;
	f3dAddTFunPoint(tf, &aux);

	return tf;
}

/** allocate an empty transfer function (no control points)
 */
f3dTFun* f3dNewTFRGBA(void)
{
	f3dTFRGBA *tf;

	if(!(tf = MyCalloc(1, f3dTFun))){
		fprintf(stderr,"f3dAllocateTFun: Allocation Error 1\n");
		return NULL;
	}

	tf->len = 0;
	return tf;
}

/** add a point to the transfer function 
 points are kept sorted
 */
int f3dAddTFunPoint(f3dTFun *tf, f3dTFPoint *p)
{
	tf->p = MyRealloc(tf->p, tf->len+1, f3dTFPoint);
	if(tf->p == NULL){
		fprintf(stderr,"f3dAddTFunPoint: Allocatin Error\n");
		return -1;
	}
	tf->p[tf->len] = *p;

	tf->len++;
	qsort(tf->p, tf->len, sizeof(f3dTFPoint), TFPointComp);

	return 0;
}

/** delete point in the transfer function 
 the first and last point cannot be deleted
param tf  the transfer function
param pos point to delete
return number of control points
 */
int f3dDelTFunPoint(f3dTFun *tf, int pos)
{
	int i;

	if(tf->len == 0 || pos < 0 || pos > tf->len-1) return;

	/* delete the point now */
	for(i=pos; i<tf->len-1; i++){
		tf->p[i] = tf->p[i+1];
	}
	tf->len--;
	tf->p = MyRealloc(tf->p, tf->len, f3dTFPoint);
	if(tf->p == NULL){
		fprintf(stderr,"f3dDelTFunPoint: Allocatin Error\n");
		return -1;
	}

	return 0;
}

/** delete the transfer function structure 
 The structure remains in the same state, as if created by f3dNewTFun()
 */
int f3dFreeTFun(f3dTFun *tf)
{
	if (tf->p) free(tf->p);
	free(tf);

	return 0;
}
