/* f3dtype.cpp
 *
 * This program returns value of various f3d header fields
 *
 * This file is part of an f3d package, which was designed and partialy also
 * implemented during the author's post doctoral stay at SUNY an Stony Brook,
 * in the lab of Prof. Arie Kaufman.
 * 
 * For details, see the f3d World-Wide-Web page,
 * http://www.viskom.oeaw.ac.at/~milos/page/Download.html
 * or send a mail to milos.sramek@oeaw.ac.at
 * 
 * Copyright (C) 1999 Free Software Foundation
 * Milos Sramek  <milos.sramek@oeaw.ac.at>
 * 
 * f3d package is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * the f3d package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with the f3d package -- see the file COPYING.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include <f3d.h>
#include <stdio.h>

/* VS --> */
//#include <f3dabstream.h>
/* VS <-- */

/*
 *  global parameters
 */
char *progname;
#include "version.h"

enum types {xnbands, xdtype, xvtype, cmdf, cmdi};
enum types  info = cmdf;

void usage(void)
{
	version(progname);
	fprintf(stderr,
			"%s --- print information about the volume (as cmd line switches) or value of various header fields\n",progname);
	fprintf(stderr,"usage: %s [switches] [input_file]\n",progname);
	fprintf(stderr,"switches:\n");
	fprintf(stderr,"\t-h --help .......... this usage\n");
	fprintf(stderr,"\t-cmdf3d ............ command line switches for raw2f3d (default)\n");
	fprintf(stderr,"\t-cmdimgcnv ......... command line switches for imgcnv)\n");
	fprintf(stderr,"\t-nbands ............ number of bands\n");
	fprintf(stderr,"\t-dtype ............. voxel value type\n");
	fprintf(stderr,"\t-vtype ............. grid type\n");
	exit(1);
}

int main(int argc, char *argv[])
{
	FILE  *fin;
	f3dHeader hdr;
	const char *tn;

	progname = argv[0];

	for (--argc, ++argv; argc > 0; )
		if (strcmp(*argv,"-h") == 0 || strcmp(*argv,"--help") == 0  ) {
			usage();
		} else if (strcmp(*argv,"-v") == 0 || strcmp(*argv,"--version") == 0  ) {
			version(progname);
			exit(1);
		} else if (strncmp(*argv,"-cmdf", 5) == 0) {
			info = cmdf;
			++argv;
			argc--;
		} else if (strncmp(*argv,"-cmdi", 5) == 0) {
			info = cmdi;
			++argv;
			argc--;
		} else if (strncmp(*argv,"-dtype", 2) == 0) {
			info = xdtype;
			++argv;
			argc--;
		} else if (strncmp(*argv,"-vtype", 2) == 0) {
			info = xvtype;
			++argv;
			argc--;
		} else if (strncmp(*argv,"-nbands", 2) == 0) {
			info = xnbands;
			++argv;
			argc--;
		} else {
			break;
		}


	if(argc == 1){  
		/* open the file */
		if ((fin = fopen(argv[0],"rb")) == NULL) {
			fprintf(stderr,"%s: ERROR -- %s\n", progname, f3dErrorMsg(F3D_FILE_OPEN));
			exit(1);
		}
	} else if (argc == 0){
		fin = stdin;
	} else {
		fprintf(stderr,"%s: ERROR -- Incorrect number of arguments \n", progname);
		usage();
		exit(1);
	}

	// load volume header

	if(f3dReadHeaderF(fin, &hdr) == NULL){
		fprintf(stderr,"%s: ERROR -- %s\n", progname, f3dErrorMsg(F3D_SHOW_ERROR));
		exit(1);
	}


	int bits;
	if(info == cmdf)
		switch(hdr.dtype){
		case f3dUCharType: tn = "uchar"; break;
		case f3dCharType: tn = "char"; break;
		case f3dUInt12Type: tn = "uint12"; break;
		case f3dIntHUType: tn = "inthu"; break;
		case f3dUInt16Type: tn = "uint16"; break;
		case f3dInt16Type: tn = "int16"; break;
		case f3dUInt32Type: tn = "uint32"; break;
		case f3dInt32Type: tn = "int32"; break;
		case f3dFloatType: tn = "float"; break;
		case f3dDoubleType: tn = "double"; break;
		}
	else if (info == cmdi){
		switch(hdr.dtype){
		case f3dUCharType: tn = "uint8"; bits=8; break;
		case f3dCharType: tn = "int8"; bits=8; break;
		case f3dUInt12Type: 
		case f3dUInt16Type: tn = "uint16"; bits=16; break;
		case f3dIntHUType: 
		case f3dInt16Type: tn = "int16"; bits=16; break;
		case f3dUInt32Type: tn = "uint32"; bits=32; break;
		case f3dInt32Type: tn = "int32"; bits=32; break;
		case f3dFloatType: tn = "float"; bits=32; break;
		case f3dDoubleType: tn = "double"; bits=64; break;
		}
	}

	switch(info){
	case xnbands: printf("%d",hdr.nbands); break;
	case xdtype: printf("%d",hdr.dtype); break;
	case xvtype: printf("%d %d",hdr.vtype, hdr.nblocks); break;
	case cmdf:
		     printf("-t %s -s %d %d %d -b %d", tn, hdr.nx, hdr.ny, hdr.nz, hdr.nbands); break;
	case cmdi:
		     printf("%d,%d,%d,%d,%d,%d,%s", hdr.nx, hdr.ny, hdr.nbands, bits, hdr.nz, (hdr.endian==f3dLittleEndian)?1:0, tn);
		     break;
	}
	return -1;

}

/*! \file f3dtype.cpp
  \brief This program prints value of the requested numerical f3d header field.

Usage: {\tt f3dtype [switches] [input_file]}
If input_file is not specified, file is read from standard input.

Printed values should correspond to their definition in f3.h

Switches:
@param {\bf -nbands} number of bands
@param {\bf -dtype} info about voxel value (char, uchar,...) type
@param {\bf -h} usage
@param {\bf --help} usage
@param {\bf -v} version
@param {\bf --version} version

@author Milos Sramek
*/
