#include <f3d/f3d.h>

char *progname="";
void usage(void);

// konverzia f3dInt16 -> f3Float, pomocou f3dReadGrid
int cvtgrid(const char *source, const char *dest)
{
	f3dHeader hdr;
	f3dInt16 *f; // typ musíme vediet vopred	
	int i;

	if (f3dReadGrid((char *)source, (void*)&f, &hdr) != 0){
		fprintf(stderr, "%s: grid read error.\n", progname);
		usage();
		return 1;
	}

	f3dInt16 fmin = f3dInt16Max;
	f3dInt16 fmax = f3dInt16Min;
	int size = hdr.nx*hdr.ny*hdr.nz;
	for (i=0; i< size; i++){
		if (f[i] < fmin) fmin = f[i];
		if (f[i] > fmax) fmax = f[i];
	} 
	f3dFloat *ff = (f3dFloat*) malloc(size*sizeof(f3dFloat));
	if (!ff) {
		fprintf(stderr, "Cannot allocate %ld bytes\n", size*sizeof(f3dUInt16)); 
		exit(1);
	}
	for (i=0; i< size; i++){
		ff[i] = (f3dFloat)((f[i] - fmin)) / (fmax-fmin);
	}

	hdr.dtype = f3dFloatType;
	f3dWriteGrid((char*)dest, ff, &hdr);
}

// konverzia f3dInt16 -> f3Float, pomocou f3dReadCubGrid
int cvtcub(const char *source, const char *dest)
{
	int xsize, ysize, zsize, nbands;
	char **txt;
	int ntxt;
	f3dDataType dtype;
	f3dInt16 *f; // typ musíme vediet vopred	
	int i;

	if (f3dReadCubGrid((char *)source, (void **)&f,   &xsize, &ysize, &zsize, &nbands, &dtype, &txt, &ntxt)){
		fprintf(stderr, "%s: grid read error.\n", progname);
		usage();
		return 1;
	}

	f3dInt16 fmin = f3dInt16Max;
	f3dInt16 fmax = f3dInt16Min;
	int size = xsize*ysize*zsize;
	for (i=0; i< size; i++){
		if (f[i] < fmin) fmin = f[i];
		if (f[i] > fmax) fmax = f[i];
	}

	f3dFloat *ff = (f3dFloat*) malloc(size*sizeof(f3dFloat));
	if (!ff) {
		fprintf(stderr, "Cannot allocate %ld bytes\n", size*sizeof(f3dUInt16)); 
		exit(1);
	}
	for (i=0; i< size; i++){
		ff[i] = (f3dFloat)((f[i] - fmin)) / (fmax-fmin);
	}

	f3dWriteCubGrid((char *)dest, ff,   xsize, ysize, zsize, nbands, f3dFloatType, txt, ntxt);

}

const char *source="in.f3d";
const char *dest="out.f3d";

void usage(void)
{
	fprintf(stderr,
			"%s --- f3d functionality test by copying input to output by two methods\n",progname);
	fprintf(stderr,"usage: %s [switches] [input_file]\n",progname);
	fprintf(stderr,"switches:\n");
	fprintf(stderr,"\t-h --help ......... this usage\n");
	fprintf(stderr,"\t-in    ............ input file (%s)\n", source);
	fprintf(stderr,"\t-out   ............ output file (%s)\n", dest);
	fprintf(stderr,"\t-cub   ............ text the 'f3d..Cub...' functions (f3d..Grid... functions)\n");
	exit(1);
}

int main(int argc, char *argv[])
{
	progname = argv[0];
	int testcub = 0;

	for (--argc, ++argv; argc > 0; )
		if (strcmp(*argv,"-h") == 0 || strcmp(*argv,"--help") == 0  ) {
			usage();
			exit(1);
		} else if (strncmp(*argv,"-c", 2) == 0) {
			testcub=1;
			++argv;
			argc--;
		} else if (strcmp(*argv,"-i") == 0) {
			source = *++argv;
			++argv;
			argc -= 2;
		} else if (strcmp(*argv,"-o") == 0) {
			dest = *++argv;
			++argv;
			argc -= 2;
		} else {
			break;
		}

	if (testcub)
		cvtcub(source, dest);
	else
		cvtgrid(source, dest);
}
