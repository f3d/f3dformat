/* f3dabstream.c
 *
 * Implementation of ``abstract stream'' providing common interface to files and buffers.
 *
 * This file is part of an f3d package, which was designed and partialy also
 * implemented during the author's post doctoral stay at SUNY an Stony Brook,
 * in the lab of Prof. Arie Kaufman.
 *
 * For details, see the f3d World-Wide-Web page,
 * http://www.viskom.oeaw.ac.at/~milos/page/Download.html
 * or send a mail to milos.sramek@oeaw.ac.at
 *
 * Copyright (C) 1999 Free Software Foundation
 * Milos Sramek  <milos.sramek@oeaw.ac.at>
 *
 * f3d package is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * the f3d package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with the f3d package -- see the file COPYING.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

 /* This file written by Viliam Solcany <viliam.solcany@oeaw.ac.at>, 2007.*/


#include <stdio.h>
#include <stdlib.h>

#include "thb.h"
#include "f3dabstream.h"

//#define DEBUG_ABF_REG	//register and check abfstreams, useful for debugging
#ifdef DEBUG_ABF_REG
#undef DEBUG_ABF_REG
#endif

#define FPTRREG_MAX 64
void *fptrreg[FPTRREG_MAX] = {0};
int isreg(const void *ap, const char *where)
{
	int i;
	int rtn = 0;
	for(i = 0; i < FPTRREG_MAX; i++) {
		if(fptrreg[i] == ap) {
			rtn = 1;
			break;
		}
	}
	if(!rtn && where) {
		fprintf(stderr, "WARNING: ptr 0x%08x not registered as abfptr in %s\n", ap, where);
	}
	return rtn;
}

/*implemetation*/
size_t abfspace_w(FILE *stream)
{
	int rtn;
	int rgtd = 0;
	int tst;
	ABSTREAM *abs;

	abs = (ABSTREAM *)stream;
#ifdef DEBUG_ABF_REG
	rgtd = isreg(abs, "abfspace_w");
#else
	rgtd = isreg(abs, NULL);
#endif
	tst = rgtd ? abs->type : W_FILE; /* if not registered (obtained through fopen, not abfopen), it is considered W_FILE */
	switch(tst/*abs->type*/) {
		case W_FILE:
			fprintf(stderr, "function ab_space_w not applicable to FILE* streams\n");
			rtn = EOF;
			break;
		case W_USSB:
			fprintf(stderr, "ERROR: USSB buffers not supported\n");
			rtn = EOF;
			/*rtn = ussb_space_w(abs->ussbptr);*/
			break;
		case W_THB:
			rtn = thb_space_w(abs->thbptr);
			break;
		default:
			fprintf(stderr, "WARNING: Invalid stream type %d in abfspace_w\n", abs->type);
			rtn = EOF; /*-1;*/
	}
	return rtn;
}

size_t abfspace_r(FILE *stream)
{
	int rtn, rgtd = 0, tst;
	ABSTREAM *abs = (ABSTREAM *)stream;

#ifdef DEBUG_ABF_REG
	rgtd = isreg(abs, "abfspace_r");
#else
	rgtd = isreg(abs, NULL);
#endif
	tst = rgtd ? abs->type : W_FILE; /* if not registered (obtained through fopen, not abfopen), it is considered W_FILE */

	switch(tst) {
		case W_FILE:
			fprintf(stderr, "function ab_space_r not applicable to FILE* streams\n");
			rtn = EOF;
			break;
		case W_USSB:
			fprintf(stderr, "ERROR: USSB buffers not supported\n");
			rtn = EOF;
			/*rtn = ussb_space_r(abs->ussbptr);*/
			break;
		case W_THB:
			rtn = thb_space_r(abs->thbptr);
			break;
		default:
			fprintf(stderr, "WARNING: Invalid stream type %d in abfspace_r\n", abs->type);
			rtn = EOF; /*-1;*/
	}
	return rtn;
}

/* todo: add imode and omode params so that std pointers may point to ABSTREAM wrapping any available kind of stream */
int abfstdd(const char *si, FILE **in_rplc, const char *so, FILE **out_rplc)
{
	int i;
	ABSTREAM *abs;
	FILE *fp;

	/*for stdin*/
	if(si == NULL) { /* working with regular stdin, but it needs to be wrapped in an ABSTREAM */
		abs = (ABSTREAM *)malloc(sizeof(ABSTREAM));
		if(!abs) {
			fprintf(stderr, "malloc failed in stdd\n");
			return -1;
		}
		abs->type = W_FILE;
		abs->fptr = stdin;
		*in_rplc = (FILE *)abs;
/*#ifdef DEBUG_ABF_REG*/
		/* register the new ptr */
		for(i = 0; i < FPTRREG_MAX; i++) {
			if(fptrreg[i] == NULL) {
				fptrreg[i] = (void *)abs;
				break;
			}
		}
		if(i == FPTRREG_MAX) {
			fprintf(stderr, "WARNING: Register of fptr overflowed\n");
		}
/*#endif*/
	} else {
		fp = abfopen(si, "trb");	/*this creates ABSTREAM (and registers it in fptrreg), ``si'' is the name */
		if(!fp) return -1;
		*in_rplc = fp;
		if(!(((ABSTREAM *)*in_rplc)->type == W_THB)) {
			fprintf(stderr, "Inconsistent abstream in abfstdd\n");
			return -1;
		}
	}
	/*for stdout*/
	if(so == NULL) {
		abs = (ABSTREAM *)malloc(sizeof(ABSTREAM));
		if(!abs) {
			fprintf(stderr, "malloc failed in stdd\n");
			return -1;
		}
		abs->type = W_FILE;
		abs->fptr = stdout;
		*out_rplc = (FILE *)abs;

/*#ifdef DEBUG_ABF_REG*/
		/* register the new ptr */
		for(i = 0; i < FPTRREG_MAX; i++) {
			if(fptrreg[i] == NULL) {
				fptrreg[i] = (void *)abs;
				break;
			}
		}
		if(i == FPTRREG_MAX) {
			fprintf(stderr, "WARNING: Register of fptr overflowed\n");
		}
/*#endif*/
	} else {
		fp = abfopen(so, "twb");	/*this creates ABSTREAM*/
		if(!fp) return -1;
		*out_rplc = fp;
		if(!(((ABSTREAM *)*out_rplc)->type == W_THB)) {
			fprintf(stderr, "Inconsistent abstream in stdd\n");
			return -1;
		}
	}
	return 0;
}

FILE *abfopen(const char *name, const char *mode)
{
	int i, shmid;
	char *acp;
	ABSTREAM *abs;
#if 0
	if(*mode != 's' && *mode != 'u' && *mode != 't') {  		/* s=standard, u=ussb, t=thb */
		fprintf(stderr, "ERROR: invalid mode in abfopen\n");
		return NULL;
	}
#endif
	abs = (ABSTREAM *)malloc(sizeof(ABSTREAM));
	if(abs == NULL) {fprintf(stderr, "ERROR: abfopen: malloc failed\n"); return NULL;}

	switch(*mode) {
		case 's': 
			abs->type = W_FILE; 
			abs->fptr = fopen(name, mode+1);
			break;
		case 't': 
			abs->type = W_THB; 
			abs->thbptr = thb_open(name, mode+1);
			break;
		default: abs->type = W_NONE; break;
	};
	if(abs->type == W_NONE) {  /* if not decided according to mode (1st letter), it can be FILE or USSB */
	        shmid = strtol(name, &acp, 10);
		if(*acp == '\0') {
			abs->type = W_USSB;
			fprintf(stderr, "ERROR: USSB buffers not supported\n");
			abs->ussbptr = NULL; /* ussb not supported */
			/*abs->ussbptr = ussb_open(shmid, mode);*/
		} else {
			abs->type = W_FILE;
			abs->fptr = fopen(name, mode);
		}
	}
	if(abs->fptr == NULL) { /* this tests all cases */
		free(abs);
		abs = NULL;
	} else { /* register the new ptr */
		for(i = 0; i < FPTRREG_MAX; i++) {
			if(fptrreg[i] == NULL) {
				fptrreg[i] = (void *)abs;
				break;
			}
		}
		if(i == FPTRREG_MAX) {
			fprintf(stderr, "WARNING: Register of fptr overflowed\n");
		}
	}
	return (FILE *)abs;
}

int abfclose(FILE *stream) 
{
	int i, rtn, rgtd = 0;
	ABSTREAM *abs;

#ifdef DEBUG_ABF_REG
	rgtd = isreg(stream, "abfclose");
#else
	rgtd = isreg(stream, NULL);
#endif
	if(!rgtd) return EOF;	/* abfclose should not be called for non-registered */

	abs = (ABSTREAM *)stream;
	switch(abs->type) {
		case W_FILE:
			rtn = fclose(abs->fptr); 
			break;
		case W_USSB:
			fprintf(stderr, "ERROR: USSB buffers not supported\n");
			rtn = EOF;
			/*rtn = ussb_close(abs->ussbptr);*/
			break;
		case W_THB:
			rtn = thb_close(abs->thbptr); 
			break;
		default:
			fprintf(stderr, "WARNING: Invalid stream type %d in abfclose\n", abs->type);
			rtn = EOF; /*-1;*/
	}
	/* "unregister" the ptr */
	for(i = 0; i < FPTRREG_MAX; i++) {
		if(fptrreg[i] == (void *)abs) {
			fptrreg[i] = NULL;
			break;
		}
	}
	if(i == FPTRREG_MAX) {
		fprintf(stderr, "WARNING: abfclose: fptr has not been registered\n");
	}
	free(abs);
	abs = NULL;
	return rtn;
}

size_t abfread(void *ptr, size_t size, size_t n, FILE *stream)
{
	size_t rtn;
	size_t rgtd = 0;
	ABSTREAM *abs;

	abs = (ABSTREAM *)stream;

#ifdef DEBUG_ABF_REG
	rgtd = isreg(abs, "abfread");
#else
	rgtd = isreg(abs, NULL);	/* silent */
#endif
	if(!rgtd) { /* if not registered (obtained through fopen, not abfopen), it is considered W_FILE */
		return fread(ptr, size, n, stream);
	}
	/*fprintf(stderr, "DEBUG: abfread called with size=%d, n=%d, stream->type=%d\n", size, n, abs->type);*/
	switch(abs->type) {
		case W_FILE:
			rtn = fread(ptr, size, n, abs->fptr); 
			break;
		case W_USSB: 
			fprintf(stderr, "ERROR: USSB buffers not supported\n");
			rtn = 0;
			/*rtn = ussb_read(ptr, size, n, abs->ussbptr); */
			break;
		case W_THB:
			rtn = thb_read(ptr, size, n, abs->thbptr); 
			break;
		default:
			fprintf(stderr, "WARNING: Invalid stream type %d in abfread\n", abs->type);
			rtn = 0;  /* todo: what is the return value of fread? */
	}
	return rtn;
}

size_t abfwrite(const void *ptr, size_t size, size_t n, FILE *stream)
{
	size_t rtn, rgtd = 0;
	ABSTREAM *abs;

	abs = (ABSTREAM *)stream;

#ifdef DEBUG_ABF_REG
	rgtd = isreg(abs, "abfwrite");
#else
	rgtd = isreg(abs, NULL);
#endif
	if(!rgtd) {
		return fwrite(ptr, size, n, stream); 
	}
	switch(abs->type) {
		case W_FILE:
			rtn = fwrite(ptr, size, n, abs->fptr); 
			break;
		case W_USSB:
			fprintf(stderr, "ERROR: USSB buffers not supported\n");
			rtn = 0;
			/*rtn = ussb_write(ptr, size, n, abs->ussbptr);*/
			break;
		case W_THB:
			rtn = thb_write(ptr, size, n, abs->thbptr); 
			break;
		default:
			fprintf(stderr, "WARNING: Invalid stream type %d in abfwrite\n", abs->type);
			rtn = 0;  /* todo: what is the return value of fwrite? */
	}
	return rtn;
}

int abfflush(FILE *stream)
{
	size_t rtn, rgtd = 0;
	ABSTREAM *abs;

	abs = (ABSTREAM *)stream;

#ifdef DEBUG_ABF_REG
	rgtd = isreg(abs, "abfflush");
#else
	rgtd = isreg(abs, NULL);
#endif
	if(!rgtd) {
		return fflush(stream);
	}
	switch(abs->type) {
		case W_FILE:
			rtn = fflush(abs->fptr); 
			break;
		case W_USSB:
			fprintf(stderr, "ERROR: USSB buffers not supported\n");
			rtn = EOF;
			/*rtn = ussb_fflush(abs->ussbptr); */
			break;
		case W_THB:
			rtn = thb_fflush(abs->thbptr); 
			break;
		default:
			fprintf(stderr, "WARNING: Invalid stream type %d in abfflush\n", abs->type);
			rtn = EOF; /*-1;  */
	}
	return rtn;
}

char *abfgets(char *s, int size, FILE *stream)
{
	char *rtn, rgtd = 0;
	ABSTREAM *abs;

	abs = (ABSTREAM *)stream;

#ifdef DEBUG_ABF_REG
	rgtd = isreg(abs, "abfgets");
#else
	rgtd = isreg(abs, NULL);
#endif
	if(!rgtd) {
		return fgets(s, size, stream);
	}
	switch(abs->type) {
		case W_FILE:
			rtn = fgets(s, size, abs->fptr); 
			break;
		case W_USSB:
			fprintf(stderr, "ERROR: USSB buffers not supported\n");
			rtn = NULL;
			/*rtn = ussb_fgets(s, size, abs->ussbptr); */
			break;
		case W_THB:
			rtn = thb_fgets(s, size, abs->thbptr); 
			break;
		default:
			fprintf(stderr, "WARNING: Invalid stream type %d in abfgets\n", abs->type);
			rtn = NULL;
	}
	/*fprintf(stderr, "DBG (%d) abfgets(%s):\"%s\"\n----\n", getpid(), abs->type == W_FILE ? "file":"ussb", rtn);*/
	return rtn;
}

int abfprintf(FILE *stream, const char *format, ...)
{
	int rtn, rgtd = 0;
	ABSTREAM *abs;
	va_list vaptr;

	abs = (ABSTREAM *)stream;

#ifdef DEBUG_ABF_REG
	rgtd = isreg(abs, "abfprintf");
#else
	rgtd = isreg(abs, NULL);
#endif
	va_start(vaptr, format);
	if(!rgtd) {
		rtn = vfprintf(stream, format, vaptr);
	} else {
		switch(abs->type) {
			case W_FILE:
				rtn = vfprintf(abs->fptr, format, vaptr); 
				break;
			case W_USSB:
				fprintf(stderr, "ERROR: USSB buffers not supported\n");
				rtn = EOF;
				/*rtn = ussb_vfprintf(abs->ussbptr, format, vaptr); */
				break;
			case W_THB:
				rtn = thb_vfprintf(abs->thbptr, format, vaptr); 
				break;
			default:
				fprintf(stderr, "WARNING: Invalid stream type %d in abfprintf\n", abs->type);
				rtn = EOF;
		}
	}
	va_end(vaptr);
	return rtn;
}

/* these functions are wrappers of thb_ functions that need to be callable directly from user code */
int thbw_create(const char *name, int block_sz, int num_blocks, int dbg)
{
	return thb_create(name, block_sz, num_blocks, dbg);
}

int thbw_destroy(FILE *stream) //(void const *bptr);
{

	int rtn;
	int rgtd = 0;
	ABSTREAM *abs;

	abs = (ABSTREAM *)stream;
#ifdef DEBUG_ABF_REG
	rgtd = isreg(abs, "thbw_destroy");
#else
	rgtd = isreg(abs, NULL);
#endif
	if(!rgtd) return -1; /* this function is only for thb streams */
		
	switch(abs->type) {
		case W_THB:
			rtn = thb_destroy(abs->thbptr);
			break;
		default:
			fprintf(stderr, "ERROR: Invalid stream type %d in thbw_destroy\n", abs->type);
			rtn = -1;
	}
	return rtn;

}

int thbw_regal1empty(FILE* stream, msem_t *s1e) //(const void *bptr, msem_t *s1e);
{

	int rtn;
	int rgtd = 0;
	ABSTREAM *abs;

	abs = (ABSTREAM *)stream;
#ifdef DEBUG_ABF_REG
	rgtd = isreg(abs, "thbw_regal1empty");
#else
	rgtd = isreg(abs, NULL);
#endif
	if(!rgtd) {
		fprintf(stderr, "ERROR: stream not registered in thbw_regal1empty\n");
		return -1; /* this function is only for registered streams */
	}
	switch(abs->type) {
		case W_THB:
			rtn = thb_regal1empty(abs->thbptr, s1e);
			break;
		default:
			fprintf(stderr, "ERROR: Invalid stream type %d in thb_regal1empty\n", abs->type);
			rtn = -1;
	}
	return rtn;
}

int thbw_regal1full(FILE* stream, msem_t *s1f) //(const void *bptr, msem_t *s1f);
{

	int rtn;
	int rgtd = 0;
	ABSTREAM *abs;

	abs = (ABSTREAM *)stream;
#ifdef DEBUG_ABF_REG
	rgtd = isreg(abs, "thbw_regal1full");
#else
	rgtd = isreg(abs, NULL);
#endif
	if(!rgtd) {
		fprintf(stderr, "ERROR: stream not registered in thbw_regal1full\n");
		return -1; /* this function is only for registered thb streams */
	}
	switch(abs->type) {
		case W_THB:
			rtn = thb_regal1full(abs->thbptr, s1f);
			break;
		default:
			fprintf(stderr, "ERROR: Invalid stream type %d in thb_regal1full\n", abs->type);
			rtn = -1;
	}
	return rtn;
}

