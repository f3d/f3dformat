/* thb.c
 *
 * Implementation of buffers (called thb) for thread communication.
 *
 * This file is part of an f3d package, which was designed and partialy also
 * implemented during the author's post doctoral stay at SUNY an Stony Brook,
 * in the lab of Prof. Arie Kaufman.
 *
 * For details, see the f3d World-Wide-Web page,
 * http://www.viskom.oeaw.ac.at/~milos/page/Download.html
 * or send a mail to milos.sramek@oeaw.ac.at
 *
 * Copyright (C) 1999 Free Software Foundation
 * Milos Sramek  <milos.sramek@oeaw.ac.at>
 *
 * f3d package is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * the f3d package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with the f3d package -- see the file COPYING.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

 /* This file written by Viliam Solcany <viliam.solcany@oeaw.ac.at>, 2007.*/

#include <stdio.h>

// because of page size system dependent info
#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

#include <string.h>
#include <errno.h>
#include <malloc.h>
#include <pthread.h>

#include "f3dmsem.h"
#include "thb.h"
/* semaphore implementation based on condition variables, inspired by www.cosy.sbg.ac.at/~pmeerw/Threads/pthreads.ps.gz */
int msem_init(msem_t *sem, int pshared, unsigned int value)  /* pshared is there for compatibility with posix sem, here ignored */
{
	int rtn;
	rtn = pthread_mutex_init(&sem->lock, NULL);
	if(rtn != 0) {
		perror("pthread_mutex_init");
		return -1;
	}
	rtn = pthread_cond_init(&sem->cond, NULL);
	if(rtn != 0) {
		perror("pthread_cond_init");
		return -1;
	}
	sem->count = value < 0 ? 0 : value;

	return 0;
}

int msem_destroy(msem_t *sem)
{
	int rtn;
	rtn = pthread_cond_destroy(&sem->cond);
	if(rtn != 0) {
		perror("pthread_cond_destroy");
		return -1;
	}
	rtn = pthread_mutex_destroy(&sem->lock);
	if(rtn != 0) {
		perror("pthread_mutex_destroy");
		return -1;
	}
	return 0;
}

int msem_wait(msem_t *sem)
{
	int rtn;
	rtn = pthread_mutex_lock(&sem->lock);
	if(rtn != 0) {
		perror("pthread_mutex_lock");
		return -1;
	}
	while(sem->count == 0) {
		rtn = pthread_cond_wait(&sem->cond, &sem->lock);
		if(rtn != 0) {
			perror("pthread_cond_wait");
			return -1;
		}
	}
	sem->count--;
	rtn = pthread_mutex_unlock(&sem->lock);
	if(rtn != 0) {
		perror("pthread_mutex_unlock");
		return -1;
	}
	return 0;
}

int msem_post(msem_t *sem)
{
	int contention, rtn;
	rtn = pthread_mutex_lock(&sem->lock);
	if(rtn != 0) {
		perror("pthread_mutex_lock");
		return -1;
	}
	contention = sem->count++ == 0;
	rtn = pthread_mutex_unlock(&sem->lock);
	if(rtn != 0) {
		perror("pthread_mutex_unlock");
		return -1;
	}
	if(contention) {
		rtn = pthread_cond_signal(&sem->cond);
		if(rtn != 0) {
			perror("pthread_cond_signal");
			return -1;
		}
	}
	return 0;
}

int msem_wait_q(msem_t *sem)
{
	int rtn;
	rtn = pthread_mutex_lock(&sem->lock);
	if(rtn != 0) {
		perror("pthread_mutex_lock");
		return -1;
	}
	sem->count = 1; /* means that process waits */
	while(sem->count) {
		rtn = pthread_cond_wait(&sem->cond, &sem->lock);
		if(rtn != 0) {
			perror("pthread_cond_wait");
			return -1;
		}
	}
	rtn = pthread_mutex_unlock(&sem->lock);
	if(rtn != 0) {
		perror("pthread_mutex_unlock");
		return -1;
	}
	return 0;
}

int msem_post_q(msem_t *sem)
{
	int rtn;
	rtn = pthread_mutex_lock(&sem->lock);
	if(rtn != 0) {
		perror("pthread_mutex_lock");
		return -1;
	}
	if(sem->count) {  /* if the caller of wait is waiting */
		sem->count = 0; /* release it */
		rtn = pthread_cond_signal(&sem->cond);
	}
	rtn = pthread_mutex_unlock(&sem->lock);
	if(rtn != 0) {
		perror("pthread_mutex_unlock");
		return -1;
	}
	return 0;
}

#define MAX_BUFF_BLOCKS 1024
/*#define YIELD_PROC sched_yield()*/

#define THBREG_MAX 32
#define THBNAME_MAX 64

static struct {
	char name[THBNAME_MAX];
	void *ptr;
} thbreg[THBREG_MAX] = {0};

typedef struct {
	/*"const" members */
	void *this;
	int regind;	/*index in Thbreg*/
	unsigned char *dstart;	/*address of the start of data */
	int num_blocks;	/*actual # buffer blocks, <= MAX_BUFF_BLOCKS*/
	int block_sz;
	int pagesize;	/*remember it here so that getpagesize() does not need to be called everytime */
	int dbg;
	/*Buffi_t buffi; */

	/*semaphores  */
        /*sem_t s_mutex;	
        sem_t s_al1e;
        sem_t s_al1f;*/

        int read_ind;	/*indeces of blocks for reading and writing */
        int write_ind;
	struct {
		int offset;	/* offset from the block start */
		int len;	/* len in bytes */
		/* for sobws */
		msem_t isem;     /* block empty */
		msem_t isfu;	/* block full */
	} act_data[MAX_BUFF_BLOCKS];

	/* these pointers, if non--null, are used as explained below; 
	the semaphores creates the user of the buffer, not the buffer itself */
	msem_t *al1empty;	/*here the buffer signals emptying a block*/
	msem_t *al1full ;	/*here the buffer signals filling a block*/

	int nreaders;
	int nwriters;
	/*
	int num_empty;
	int num_full;*/
} Thb_t;

#ifdef _WIN32
/* getpagesize for windows */
long getpagesize (void) {
    static long g_pagesize = 0;
    if (! g_pagesize) {
        SYSTEM_INFO system_info;
        GetSystemInfo (&system_info);
        g_pagesize = system_info.dwPageSize;
    }
    return g_pagesize;
}

#define PAGE_SZ (getpagesize())

#else

#define PAGE_SZ (getpagesize())

#endif

/* Is buffer ``name'' registered? If yes return its index, if no -1 */
static int isthbreg(const char *name)
{
	int i;
	for(i = 0; i < THBREG_MAX; i++){
		if(!strncmp(name, thbreg[i].name, THBNAME_MAX)){
			return i;
		}
	}
	return -1;
}

/* create a thb and register it in thbreg; return its index in thbreg, or < 0 in case of failure */
/* ATTENTION: this function is not protected against concurrent use, thus it MUST NOT be invoked by multiple concurrent threads!!! */
/*static*/ int thb_create(const char *name, int block_sz, int num_blocks, int dbg)
{
	/* it does not check whether ``name'' already exists, thb_open should */
	/* it also doesn't care for mode, thb_open should */
	int i,k,si,total_sz;
	Thb_t *bptr;

	for(i = 0; i < THBREG_MAX; i++) {	/* find empty entry */
		if(thbreg[i].name[0] == '\0') {
			break;
		}
	}
	if(i == THBREG_MAX) {	/* register overflowed */
		fprintf(stderr, "ERROR: Thb register overflow\n");
		return -1;
	}
	bptr = (Thb_t *)malloc(sizeof(Thb_t));	
	if(!bptr) {
		fprintf(stderr, "ERROR: malloc in thb_create\n");
		return -1;
	}

	bptr->this = (void *)bptr;
	bptr->regind = i;	/*index in Thbreg*/
	bptr->num_blocks = num_blocks;	/*actual # buffer blocks, <= MAX_BUFF_BLOCKS*/
	if(num_blocks > MAX_BUFF_BLOCKS) {
		fprintf(stderr, "ERROR: thb_create: Required buffer capacity (%d items) beyond the maximum (%d)\n", num_blocks, MAX_BUFF_BLOCKS);
		return -1;
	}
	bptr->block_sz = block_sz;
	/*int pagesize;*/	/*remember it here so that getpagesize() does not need to be called everytime */
	bptr->dbg = dbg;

        bptr->read_ind = bptr->write_ind = 0;
	for(k = 0; k < MAX_BUFF_BLOCKS; k++) {
		bptr->act_data[k].offset = bptr->act_data[k].len = 0;
		si = msem_init(&bptr->act_data[k].isem, 0, 1);
		if(si != 0) {perror("sem_init"); return -1;}
		si = msem_init(&bptr->act_data[k].isfu, 0, 0);
		if(si != 0) {perror("sem_init"); return -1;}
	}
	bptr->nreaders = bptr->nwriters = 0;

	/* allocate the space for data */
        total_sz = num_blocks*block_sz;
	if(total_sz % PAGE_SZ) {
		total_sz += (total_sz/PAGE_SZ + 1) * PAGE_SZ;	/*round up to PAGE_SZ */
	}

	bptr->dstart = (unsigned char *)malloc(total_sz) ;	/*address of the start of data */
	if(!bptr->dstart) {
		free((void *)bptr);
		fprintf(stderr, "ERROR: thb_create: cannot allocate buffer space\n");
		return -1;
	}
	
	/* these are initially null; they are set using a separate function */
	bptr->al1empty = bptr->al1full = NULL;

	/* register it to thbreg */
	strcpy(thbreg[i].name, name);
	thbreg[i].ptr = (void *)bptr;

	return i;
}

/* returns 0 on success, <0 otherwise */
/*static*/ int thb_destroy(void *bptr)
{
	Thb_t *p;
	int i;

	p = (Thb_t *)bptr;
	if(bptr != thbreg[p->regind].ptr) {
		fprintf(stderr, "ERROR: thb_destroy: inconsistency in thb register\n");
		return -1;
	}
	if(bptr != p->this) {
		fprintf(stderr, "ERROR: thb_destroy: inconsistency in thb buffer\n");
		return -1;
	}
	if(p->nreaders != 0 || p->nwriters != 0) {
		fprintf(stderr, "There are some readers and/or writers using the buffer 0x%08x. Cannot delete it\n", bptr);
		return -1;
	}

	/* de-register */
	thbreg[p->regind].name[0] = '\0';
	thbreg[p->regind].ptr = NULL;

	free(p->dstart);
	for(i = 0; i < MAX_BUFF_BLOCKS; i++) {
		msem_destroy(&p->act_data[i].isem);
		msem_destroy(&p->act_data[i].isfu);
	}
	free(bptr);

        return 0;
}

void *thb_open(const char *name, const char *mode)
{
	/*TODO: use 'mode' */
	int ind;
	Thb_t *retptr = NULL;

	if(mode[0] != 'r' && mode[0] != 'w') {
		fprintf(stderr, "ERROR: thb_open: invalid mode\n");
		return NULL;
	}
	ind = isthbreg(name); 

	if(ind < 0) {
		fprintf(stderr, "ERROR: thb_open: buffer %s does not exist\n", name);
		return NULL;
	}
	retptr = (Thb_t *)thbreg[ind].ptr;
	if(mode[0] == 'r') {
		retptr->nreaders++;
	} else {
		retptr->nwriters++;
	}

	return retptr;
	/*TODO: if it fails, destroy what has already been created!!  */
}

int thb_close(const void *bptr)
{
	/* TODO: no way to determine if the calling thread is reader or writer, i.e. the buffer should remember thread ids in thb_open */
	Thb_t *p = (Thb_t *)bptr;
	if(p->nreaders > 0) {
		p->nreaders--;
	} else if(p->nwriters > 0) {
		p->nwriters--;
	} else {
		fprintf(stderr, "ERROR: thb_close: incorrect nreaders or nwriters\n");
		return -1;
	}
	/* if(p->nreaders == 0 && p->nwriters == 0) {
		return thb_destroy(p);
	} */	
	return 0;
}

/* this should be called BEFORE any read/write */
int thb_regal1empty(const void *bptr, msem_t *s1e)
{
	int i;
	Thb_t *p = (Thb_t *)bptr;
	p->al1empty = s1e;
	return 0;
}

int thb_regal1full(const void *bptr, msem_t *s1f)
{
	Thb_t *p = (Thb_t *)bptr;
	p->al1full = s1f;
	return 0;
}
/* ``unregister'' functions not needed */

/*
 * The buffer consists of blocks. The write function writes 'nelem' elements, each of size 'elemsize'. 
 * Each element is written into a separate buffer block.
 * Writing an element is atomic. Element size must be <= block size.
 * The read function also reads element-wise. It can read only a part of data placed in a buffer block. 
 * After reading all data of a block, the block is "released" so that it can be reused. 
 * Theoretically, any amount of data can be read as one 'read element'.
 */
size_t thb_write(const void *dataptr, size_t elemsize, size_t nelem, void const *bptr)
{
	Thb_t *p;
	int ei;
	void *wptr, *dptr;

	p = (Thb_t *)bptr;
	if(elemsize > p->block_sz) {
		fprintf(stderr, "ERROR: thb_write: elemsize %d too large (buffer block size is %d)\n", elemsize, p->block_sz);
		return 0; /*toto vyriesit inak, lepsie!? */
	}
	for(ei = 0; ei < nelem; ei++) {
		msem_wait(&p->act_data[p->write_ind].isem);
		wptr = (void *)(p->dstart + p->write_ind * p->block_sz);	/*write from block start */
		dptr = (void *)((unsigned char *)dataptr + ei * elemsize);	/*actual data ptr */
		memcpy(wptr, dptr, elemsize);

		p->act_data[p->write_ind].offset = 0;			/*write from block start */
		p->act_data[p->write_ind].len = elemsize;			/* end of ``crit. section'' */
		msem_post(&p->act_data[p->write_ind].isfu);

		p->write_ind = (p->write_ind+1) % p->num_blocks;
		if(p->al1full) msem_post_q(p->al1full);
		if(p->dbg) {	
			fprintf(stderr, "DEBUG: thb_write: memcpy to buff 0x%08x: wptr=0x%08x, elemsize=%d\n", bptr, wptr, elemsize);
		}
	}
	return nelem;
}

size_t thb_read(void const *dataptr, const size_t elemsize, const size_t nelem, const void *bptr)
{
	Thb_t *p;
	int toread, rdsf, readsz;
	void *rptr, *dptr;

	p = (Thb_t *)bptr;
	toread = elemsize * nelem;	/*amount of data read so far */
	rdsf = 0;	/*read so far */
	while(toread > 0) {
		msem_wait(&p->act_data[p->read_ind].isfu);
		rptr = (void *)(p->dstart + p->read_ind * p->block_sz + p->act_data[p->read_ind].offset); 
		dptr = (void *)((unsigned char *) dataptr + rdsf);
		readsz = toread;
		if(readsz >= p->act_data[p->read_ind].len) { /*the actual block can be released */
			readsz = p->act_data[p->read_ind].len;
			memcpy(dptr, rptr, readsz);
			p->act_data[p->read_ind].offset = 0;
			p->act_data[p->read_ind].len = 0; 
			msem_post(&p->act_data[p->read_ind].isem);

			p->read_ind = (p->read_ind+1) % p->num_blocks;
			if(p->dbg) {	
				fprintf(stderr, "DEBUG: thb_read: memcpy1 to buff 0x%08x: rptr=0x%08x, readsz=%d\n", bptr, rptr, readsz);
			}
		} else { /*part of data in the block remains unconsumed */
			memcpy(dptr, rptr, readsz);
			p->act_data[p->read_ind].offset += readsz;
			p->act_data[p->read_ind].len -= readsz; 

			msem_post(&p->act_data[p->read_ind].isfu);
			if(p->dbg) {	
				fprintf(stderr, "DEBUG: thb_read: memcpy2 to buff 0x%08x: rptr=0x%08x, readsz=%d\n", bptr, rptr, readsz);
			}
		}
		toread -=readsz;
		rdsf += readsz;
		if(p->al1empty) msem_post_q(p->al1empty);
	}
	return nelem;
}

int thb_fflush(const void *bptr)
{
	return 0;
}

/*returns amount of data [bytes] that can be written to buffer without blocking */
size_t thb_space_w(const void *bptr)
{
	Thb_t *p;
	int sz, j, ind;

	p = (Thb_t *)bptr;
	/* This fuction is called by the writer. If some of the full blocks that it has already scan become empty 
	   until it finishes scanning, the actual number is greater than that returned by this function.
	   Thus it returns a lower bound - it is safe */
	ind = p->write_ind;
	sz = 0;
	for(j = 0; j < p->num_blocks; j++) {
		if(p->act_data[ind].len == 0) sz++;
		else break;
		ind = (ind + 1) % p->num_blocks;
	}
	sz *= p->block_sz;
	return sz;
}

/*returns amount of data [bytes] that can be read from buffer without blocking*/ 
size_t thb_space_r(const void *bptr)
{
	Thb_t *p;
	int ind, totsz, j;
	
	p = (Thb_t *)bptr;
	ind = p->read_ind;
	totsz = 0;
	for(j = 0; j < p->num_blocks; j++) {
		if(p->act_data[ind].len > 0) totsz += p->act_data[ind].len;
		else break;
		ind = (ind + 1) % p->num_blocks;
	}
	return totsz;
}

char *thb_fgets(char *s, int size, const void *bptr)
{
	Thb_t *p;
	int oi, j, datacomplete, rtnnull;
	char *rptr;

	rtnnull = 0;
	p = (Thb_t *)bptr;
	oi = 0;
	datacomplete = 0;
	while(!datacomplete) {
		/* read 1 block */
		if(p->dbg) {
			fprintf(stderr, "DEBUG: thb_fgets from buff 0x%08x: read_ind=%d, act_data[read_ind].len=%d\n", 
				p, p->read_ind, p->act_data[p->read_ind].len );
		}
		msem_wait(&p->act_data[p->read_ind].isfu);
		rptr = p->dstart + p->read_ind * p->block_sz + p->act_data[p->read_ind].offset; 
		j = 0;
		while( j < p->act_data[p->read_ind].len && !datacomplete && oi < size-1) {
			s[oi] = *(rptr+j);
			if(*(rptr + j) == '\n') {
				datacomplete = 1;
			}
			if(*(rptr + j) == EOF) {
				datacomplete = 1;
				if(oi == 0) {
					rtnnull = 1;
				}
			}
			j++; oi++;
		}
		if(oi == size - 1) {
			datacomplete = 1;
		}
		if(datacomplete) {
			s[oi] = '\0';
		}
		if(j == p->act_data[p->read_ind].len) { /* release the block */
			p->act_data[p->read_ind].offset = 0;
			p->act_data[p->read_ind].len = 0; 
			msem_post(&p->act_data[p->read_ind].isem);

			p->read_ind = (p->read_ind+1) % p->num_blocks;
		} else {	/* data is complete but the current block is not totally consumed */
			p->act_data[p->read_ind].offset += j;
			p->act_data[p->read_ind].len -= j; 
			msem_post(&p->act_data[p->read_ind].isfu);
		}
	}
	if(p->dbg) {
		fprintf(stderr, "DEBUG: thb_fgets (size %d, bptr=0x%08x) getting: \"%s\"\n", size, bptr, s);
	}
	return rtnnull ? NULL : s;
}

int thb_vfprintf(void const *bptr, const char *format, va_list vaptr)
{
	Thb_t *p;
	void *wptr;
	char *abu;
	int rtn;

	p = (Thb_t *)bptr;
	if(p->dbg) {
		fprintf(stderr, "DEBUG: thb_vprintf called with bptr=0x%08x, format=\"%s\"\n", bptr, format);
	}
	abu = (char *)malloc(p->block_sz);
	if(!abu) {
		fprintf(stderr, "malloc failed in thb_vfprintf\n");
		return EOF;
	}
#ifdef _WIN32
	rtn = _vsnprintf(abu, p->block_sz, format, vaptr);
#else
	rtn = vsnprintf(abu, p->block_sz, format, vaptr);
#endif
	if(rtn >= p->block_sz) {	/* truncated!! */
		fprintf(stderr, "thb_vfprintf: data amount to write %d too large (buffer block size is %d)\n", rtn, p->block_sz);
		free(abu);
		return EOF;
	}
	msem_wait(&p->act_data[p->write_ind].isem);
	wptr = (void *)(p->dstart + p->write_ind*p->block_sz);	/*write from block start */
	memcpy(wptr, (void *)abu, rtn*sizeof(char));

	p->act_data[p->write_ind].offset = 0;
	p->act_data[p->write_ind].len = rtn;
	msem_post(&p->act_data[p->write_ind].isfu);

	p->write_ind = (p->write_ind+1) % p->num_blocks;
	free(abu);

	return rtn;
}

