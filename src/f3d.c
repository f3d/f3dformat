/* f3d.c
* implementation of the 'f3d' format

Copyright (C) 1998-1999 by Milos Sramek

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
	claim that you wrote the original software. If you use this software
	in a product, an acknowledgment in the product documentation would be
	appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
	misrepresented as being the original software. In order to not to
	confuse the users, the 'f3d' name cannot be used in the case of such
	altered versions.
3. This notice may not be removed or altered from any source distribution.


Anyway, if you would like to see some new features, please, write to the
author with your recommendations and/or wishes.
*/
#include <stdlib.h>
#include <f3d/f3d.h>
#include <zlib.h>
#include <string.h>


#ifdef HAVE_CONFIG_H
	#include "config.h"
#else //HAVE_CONFIG_H
	#error HAVE_CONFIG_H is not defined. If you''re not using cmake to build the\
 library, please create config.h file from config.h.in replacing placeholders\
 with correct values and define HAVE_CONFIG_H
#endif //HAVE_CONFIG_H


/* VS <-- */
#ifdef USE_ABSTREAM
	#include "f3dabstream.h"
#else
	#define abfread fread
	#define abfwrite fwrite
	#define abfgets fgets
	#define abfprintf fprintf
	#define abfflush fflush
	#define ISFILE(x)	(1)
#endif
/* VS <-- */

static long ReadLong(FILE *fptr);
static int WriteLong(FILE *fptr, int l);
static int SwapOrder(unsigned char *b, int l);
static int WriteSliceDataN(FILE *fptr, const void *buf, unsigned long size);
static int ReadSliceDataN(FILE *fp, void *ptr, unsigned long size);
static int WriteSliceDataZ(FILE *fptr, const void *buf, unsigned long size);
static int ReadSliceDataZ(FILE *fp, void *ptr, unsigned long size);
static FILE *ReadHeader(FILE *fptr, f3dHeader *hdr, int *nx, int *ny,
	int *color);
static int zeroHeader(f3dHeader *hdr);
static int checkHeader(f3dHeader *hdr);
static int SkipSliceData(FILE *fp);

#define MyRealloc(ptr,x,y) ((y *)realloc((ptr),(unsigned)(x) * sizeof(y)))
#define MyCalloc(x,y) (y *)calloc((unsigned)(x), sizeof(y))

#ifndef MAX
#define MAX(a,b)        (((a) > (b)) ? (a) : (b))
#endif
#ifndef MIN
#define MIN(a,b)        (((a) < (b)) ? (a) : (b))
#endif
#ifndef ABS
#define ABS(a)          (((a) > 0) ? (a) : -(a))
#endif

static int errorCode = F3D_NO_ERROR;


/* Error messages corresponding to error codes */
static char *f3dErrorMsgTxt[] = {
	"no f3d error",
	"file open error",
	"PNM header error",
	"incorrect PNM type",
	"icon image error",
	"not an f3d format file",
	"error reading PNM header line",
	"unknown 'endian' value",
	"unknown 'vtype' value",
	"unknown 'ctype' value",
	"unknown 'nbands' value",
	"unknown 'dtype' value",
	"unknown 'units' value",
	"comment line allocation error",
	"allocation error",
	"f3d format error - endian missing",
	"f3d format error - incorrect volume dimensions",
	"f3d format error - vtype missing",
	"f3d format error - units missing",
	"f3d format error - nbands missing",
	"f3d format error - dtype missing",
	"f3d format error - comments missing",
	"f3d format error - px equal 0 or missing",
	"f3d format error - py equal 0 or missing",
	"f3d format error - pz equal 0 or missing",
	"f3d format error - rx sum is 0",
	"f3d format error - ry sum is 0",
	"f3d format error - rz sum is 0",
	"f3d format error - rx equal 0",
	"f3d format error - ry equal 0",
	"f3d format error - rz equal 0",
	"f3d format error - too many transfer functions",
	"f3d format error - too few transfer functions",
	"f3d format error - transfer function array allocation error",
	"f3d format error - tfnum missing",
	"Version of the input f3d file is newer than version of your software,"
		" please upgrade"
	"f3d format error - compression type missing",
};

static char *lastcomm = "#!last";    /* last line of the f3d header */

// type out the earlier stored error message
const char * f3dErrorMsg(f3dError code)
{
	if(code == F3D_SHOW_ERROR)
		return f3dErrorMsgTxt[errorCode];
	else
		return f3dErrorMsgTxt[code];
}

/** Load the grid from a file
*/
int f3dReadGrid(char *name, void **buf, f3dHeader *hdr)
{
	FILE *fptr;
	int voxelSize, sliceSize;
	int b, z, ss;

	if ((fptr = f3dReadHeader(name, hdr)) == NULL) {
		fprintf(stderr, "f3dReadHeader error: %s (%s)\n",
				f3dErrorMsg(F3D_SHOW_ERROR), name);
		return -1;
	}

	voxelSize = f3dVoxelSize(hdr);
	sliceSize = hdr->nx * hdr->ny;
	if (buf == NULL){
		fprintf(stderr, "f3dReadGrid: Input buffer not initialized\n");
		return -1;
	}
	*buf = MyCalloc(hdr->nz * sliceSize * voxelSize * hdr->nbands,
		unsigned char);

	if(*buf == NULL) {
		fprintf(stderr, "f3dReadGrid: Allocation Error\n");
		return -1;
	}

	ss = voxelSize * sliceSize;
	for (b = 0; b < hdr->nbands; b++)
		for (z = 0; z < hdr->nz; z++)
			if (f3dReadSlice(fptr,
				(void *)((char*)*buf + (b * hdr->nz + z) * ss),
				hdr->endian, voxelSize, sliceSize,
				hdr->comptype) != 0)

				return -1;

	return 0;
}

/** Load the grid from a file
*/
int f3dReadCubGrid(char *name,
	void **buf,   /* the grid */
	int *nx, int *ny, int *nz,  /* dimensions */
	int *nbands,     /* number of data bands */
	f3dDataType *dtype,    /* data type */
	char ***txt, int *ntxt)    /* comments */
{
	f3dHeader hdr;
	int sliceSize;
	char **xtxt;
	int i;

	if ((nx == NULL) || (ny == NULL) || (nz == NULL) || (nbands == NULL) ||
		(dtype == NULL) || (txt == NULL))
	{
		fprintf(stderr, "f3dReadCubGrid:: incorrect grid parameters.\n");
		return -1;
	}

	if (f3dReadGrid(name, buf, &hdr) != 0){
		fprintf(stderr, "f3dReadCubGrid:: grid read error.\n");
		return -1;
	}

	/* get the comment lines */
	if (hdr.commentLines) {
		*ntxt = hdr.commentLines;
		if ((xtxt = MyCalloc(hdr.commentLines, char*)) == 0) {
			fprintf(stderr, "f3dReadCubGrid:: allocation error.\n");
			return -1;
		}

		for (i = 0; i < hdr.commentLines; i++) {
			if ((xtxt[i] = strdup(hdr.comment[i])) == 0) {
				fprintf(stderr, "f3dReadCubGrid:: allocation error.\n");
				return -1;
			}
		}

		*txt = xtxt;
	}

	*nx = hdr.nx;
	*ny = hdr.ny;
	*nz = hdr.nz;
	*nbands = hdr.nbands;
	*dtype = hdr.dtype;
	sliceSize = hdr.nx * hdr.ny * f3dVoxelSize(&hdr);

	return 0;
}

/*********************************************************************/
/*
*  reads header of the f3d file
*
*  char *path:  file path+name
*  f3dHeader *hdr: returned header
*  return value: - file pointer set to the start of the first slice
*          - NULL when not successfull
*
*/
FILE *f3dReadHeader(char *path, f3dHeader *hdr)
{
	FILE *fp;

	/* open the file */
	if ((fp = fopen(path, "rb")) == NULL) {
		errorCode = F3D_FILE_OPEN;
		return NULL;
	}

	return f3dReadHeaderF(fp, hdr);
}

/*********************************************************************/
/*
*  reads header of the f3d file
*
*  char *path:  file path+name
*  f3dHeader *hdr: returned header
*  return value: - file pointer set to the start of the first slice
*          - NULL when not successfull
*
*/
FILE *f3dReadHeaderF(FILE *fptr, f3dHeader *hdr)
{
	int imgWidth, imgHeight;  /* dimensions of the icon image */
	int color;
	unsigned char *img;

	if (ReadHeader(fptr, hdr, &imgWidth, &imgHeight, &color) == NULL)
		return NULL;

	/* skip the icon image */
	img = MyCalloc(imgWidth * imgHeight * (color ? 3 : 1), unsigned char);
	if (img == NULL) {
		errorCode = F3D_ALLOC_ERR;
		return NULL;
	}

	if ((int)abfread(img, imgWidth * (color ? 3 : 1), imgHeight, fptr) !=
		imgHeight)
	{
		errorCode = F3D_ICON_IMAGE_ERROR;
		return NULL;
	}

	free(img);

/* read geometry, if necessary */
/* not used since version 5.1
if(hdr->vtype == f3dStructured){
	int z;
	int zdim = ((hdr->dimInterp == f3dCellSize) ?  hdr->nz : (hdr->nz+1));
	int slicesize = ((hdr->dimInterp == f3dCellSize) ?
		hdr->nx * hdr->ny : (hdr->nx+1) * (hdr->ny+1));
	int size = slicesize * zdim;
	float *ptr;

	hdr->rx = (f3dFloat *)realloc(hdr->rx, size*sizeof(float));
	for(ptr=hdr->rx,z=0; z<zdim; z++, ptr += slicesize)
		ReadSliceData(fptr, ptr, slicesize * sizeof(float));
	hdr->ry = (f3dFloat *)realloc(hdr->ry, size*sizeof(float));
	for(ptr=hdr->ry,z=0; z<zdim; z++, ptr += slicesize)
		ReadSliceData(fptr, ptr, slicesize * sizeof(float));
	hdr->rz = (f3dFloat *)realloc(hdr->rz, size*sizeof(float));
	for(ptr=hdr->rz,z=0; z<zdim; z++, ptr += slicesize)
		ReadSliceData(fptr, ptr, slicesize * sizeof(float));
	fprintf(stderr,"Geom done\n");

	* if endian types do not match... *
	if(hdr->endian != f3dHostType()){
			for(i=0, ptr=hdr->rx; i<size; i++, ptr++)
			SwapOrder((void *)ptr,sizeof(float));
			for(i=0, ptr=hdr->ry; i<size; i++, ptr++)
			SwapOrder((void *)ptr,sizeof(float));
			for(i=0, ptr=hdr->rz; i<size; i++, ptr++)
			SwapOrder((void *)ptr,sizeof(float));
	}
}
*/

	return fptr;
}

/*********************************************************************/
/*
*  reads header of the f3d file
*
*  char *path:  file path+name
*  f3dHeader *hdr: returned header
*  int *nx : icon row length
*  int *ny : icon height
*  int *color: true is color icon
*  return value: - file pointer set to the start of the first slice
*          - NULL when not successfull
*/
FILE *f3dReadHeaderAndIcon(char *path, f3dHeader *hdr, unsigned char **img,
	int *nx, int *ny, int *color)
{
	FILE *fptr;

	/* open the file */
	if ((fptr = fopen(path, "rb")) == NULL) {
		errorCode = F3D_FILE_OPEN;
		return NULL;
	}
	return f3dReadHeaderAndIconF(fptr, hdr, img, nx, ny, color);
}

/*********************************************************************/
/*
*  reads header of the f3d file
*
*  FILE *ptr : file pointer
*  f3dHeader *hdr: returned header
*  int *nx : icon row length
*  int *ny : icon height
*  int *color: true is color icon
*  return value: - file pointer set to the start of the first slice
*          - NULL when not successfull
*/
FILE *f3dReadHeaderAndIconF(FILE *fptr, f3dHeader *hdr, unsigned char **img,
	int *nx, int *ny, int *color)
{
	fptr = ReadHeader(fptr, hdr, nx, ny, color);
	if (fptr == NULL)
		return NULL;

	/*fprintf(stderr, "DEBUG: in f3dReadHeaderAndIconF: ReadHeader returned nx=%d, ny=%d, color=%d\n", *nx, *ny, *color);*/

	/* read the icon image */
	if (fptr) {
		if (img == NULL)
			return NULL;

		*img = MyCalloc(*nx * *ny * (*color ? 3 : 1), unsigned char);
		if (*img == NULL) {
			errorCode = F3D_ALLOC_ERR;
			return NULL;
		}

		if ((int)abfread(*img, *ny * (*color ? 3 : 1), *nx, fptr) != *nx) {
			errorCode = F3D_ICON_IMAGE_ERROR;
			return NULL;
		}
	}

	return fptr;
}

int f3dReadSlice(FILE *fptr, void *buf, f3dEndianType etype, int voxelSize,
	int sliceSize, f3dCompType ctype)
{
	unsigned char *ptr;
	int i;

	if (ctype == f3dCompNone) {
		if (ReadSliceDataN(fptr, buf, sliceSize * voxelSize) != 0)
			return -1;
	} else if (ctype == f3dCompZlib) {
		if (ReadSliceDataZ(fptr, buf, sliceSize * voxelSize) != 0)
			return -1;
	}

	/* if endian types do not match... */
	if (etype != f3dHostType())
		for (i = 0, ptr = (unsigned char *)buf; i < sliceSize;
			i++, ptr += voxelSize)
		{
			if (SwapOrder(ptr, voxelSize) != 0)
				return -1;
		}

	/*fprintf(stderr,"f3dReadSlice(%d)\n", getpid());*/

	return 0;
}

int f3dSkipSlice(FILE *fptr) {
	return SkipSliceData(fptr);
}

/*
*  write 3D Cubic (Cartesian) grid of voxels into file in f3d format
*/
int f3dWriteCubGrid(char *name,
	void *buf,      /* the grid */
	int nx, int ny, int nz,    /* dimensions */
	int nbands, f3dDataType dtype,    /* grid and voxel type */
	char *txt[], int ntxt)     /* comments */
{
	int failure;

	FILE *fptr=fopen(name,"wb");

	if (fptr==NULL)
	{
		fprintf(stderr,"f3dWriteCubGrid: : %s -- File open error\n", name);
		return -1;
	}

	failure = f3dWriteCubGridF(fptr, buf, nx, ny, nz, nbands, dtype, txt, ntxt);

	fclose(fptr);

	return failure;
}

/*
*  write 3D Cubic (Cartesian) grid of voxels into file in f3d format
*/
int f3dWriteCubGridF(FILE *fptr,
void *buf,      /* the grid */
int nx, int ny, int nz,    /* dimensions */
int nbands, f3dDataType dtype,    /* grid and voxel type */
char *txt[], int ntxt)     /* comments */
{
	f3dHeader h = f3dDefaultHeader(nx, ny, nz, dtype, nbands);
	if (f3dSetHdrComment(&h, ntxt, txt) != 0)
		return -1;
	if (f3dWriteGridF(fptr, buf, &h) != 0)
		return -1;

	return 0;
}

int f3dWriteGridF(FILE *fptr, const void *buf, f3dHeader *hdr)
{
	int sliceSize = hdr->nx * hdr->ny;
	int voxelSize = f3dVoxelSize(hdr);
	int z, b, ss;
	int iw, ih;    /* icon dimensions */
	unsigned char *img;
	int failure;

	/* create the icon image */
	img = f3dCreateIconImage(buf, hdr, &iw, &ih);
	if (img == NULL)
		return -1;

	failure = f3dWriteHeader(fptr, hdr, img, iw, ih) ;
	free(img);

	if (failure)
		return -1;

	/* write position info now */
	/* not used since version 5.1
	if(hdr->vtype == f3dStructured){
	int zdim = ((hdr->dimInterp == f3dCellSize) ?  hdr->nz : (hdr->nz+1));
	int slicesize = ((hdr->dimInterp == f3dCellSize) ?
	hdr->nx * hdr->ny : (hdr->nx+1) * (hdr->ny+1));
	float *ptr;

	for(ptr=hdr->rx,z=0; z<zdim; z++, ptr += slicesize)
	WriteSliceData(fptr, ptr, slicesize*sizeof(float));
	for(ptr=hdr->ry,z=0; z<zdim; z++, ptr += slicesize)
	WriteSliceData(fptr, ptr, slicesize*sizeof(float));
	for(ptr=hdr->rz,z=0; z<zdim; z++, ptr += slicesize)
	WriteSliceData(fptr, ptr, slicesize*sizeof(float));
	}
	*/

	/* write the data now */
	ss = sliceSize * voxelSize;
	for (b = 0; b < hdr->nbands; b++)
		for (z = 0; z < hdr->nz; z++)
			if (f3dWriteSlice(fptr, (char *)buf + (b * hdr->nz + z) * ss,
				voxelSize, sliceSize, hdr->comptype) != 0)
			{
				return -1;
			}

	return 0;
}

int f3dWriteGrid(char *name, const void *buf, f3dHeader *hdr)
{
	FILE *fptr;
	int error;

	if ((fptr = fopen(name, "wb")) == 0) {
		fprintf(stderr, "f3dWriteGrid: : %s -- File open error\n", name);
		return -1;
	}

	error = f3dWriteGridF(fptr, buf, hdr);
	fclose(fptr);

	return error;
}

/*
*  write f3d header
*
*/
#define BAN 6
int f3dPrintHeader(FILE *fptr, const f3dHeader *hdr)
{
	int i, k;

	char *hh[BAN] =
		{"**************************************************************",
		"**                          f3d                             **",
		"**      Format for storage of 3D volumetric data sets       **",
		"**                                                          **",
		"**  http://www.viskom.oeaw.ac.at/~milos/page/Download.html  **",
		"**************************************************************"
		};

	if (hdr == NULL)
		return -1;


	abfprintf(fptr, "#!%s   \t%s\n","f3d", F3D_VERSION_STRING);
	for (i = 0; i < BAN; i++)
		abfprintf(fptr, "#%s\n", hh[i]);

	abfprintf(fptr, "#!endian \t%s\n",
		(hdr->endian == f3dBigEndian) ? "f3dBigEndian" :
		(hdr->endian == f3dLittleEndian) ? "f3dLittleEndian" :
		"unknown");
	abfprintf(fptr, "#!compression \t%s\n",
		(hdr->comptype == f3dCompZlib) ? "zlib" :
		(hdr->comptype == f3dCompNone) ? "none" :
		"unknown");
	abfprintf(fptr, "#!vdim  \t%d %d %d\n", hdr->nx, hdr->ny, hdr->nz);
	abfprintf(fptr, "#!vtype \t%s\n", (hdr->vtype == f3dCubic) ? "f3dCubic" :
		(hdr->vtype == f3dRegular) ? "f3dRegular" :
		(hdr->vtype == f3dRectZ) ? "f3dRectZ" :
		(hdr->vtype == f3dRectXYZ) ? "f3dRectXYZ" :
		(hdr->vtype == f3dZBlock) ? "f3dZBlock" :
		/*(hdr->vtype == f3dStructured) ? "f3dStructured" :*/
		"unknown");
	/* voxel dimensions */
	if (hdr->vtype == f3dCubic) {
		abfprintf(fptr, "#!px\t\t%f\n", hdr->px);
	} else if (hdr->vtype == f3dRegular) {
		abfprintf(fptr, "#!px\t%f\n", hdr->px);
		abfprintf(fptr, "#!py\t%f\n", hdr->py);
		abfprintf(fptr, "#!pz\t%f\n", hdr->pz);
	} else if (hdr->vtype == f3dRectZ) {
		abfprintf(fptr, "#!px\t%f\n", hdr->px);
		abfprintf(fptr, "#!py\t%f\n", hdr->py);
		for(i = 0; i < hdr->nz; i++)
			abfprintf(fptr, "#!pz\t%f\n", hdr->rz[i]);
	} else if (hdr->vtype == f3dRectXYZ) {
		for (i = 0; i < hdr->nx; i++)
			abfprintf(fptr, "#!px\t%f\n", hdr->rx[i]);
		for (i = 0; i < hdr->ny; i++)
			abfprintf(fptr, "#!py\t%f\n", hdr->ry[i]);
		for (i = 0; i < hdr->nz; i++)
			abfprintf(fptr, "#!pz\t%f\n", hdr->rz[i]);
	} else if (hdr->vtype == f3dZBlock) { //block structure
		abfprintf(fptr, "#!nblocks\t%d\n", hdr->nblocks);
		for (i = 0; i < hdr->nblocks; i++)
			abfprintf(fptr, "#!block\t%f %f %f %d\n", hdr->block[i].blockpx,
				hdr->block[i].blockpy, hdr->block[i].blockpz,
				hdr->block[i].blocknz);
	} else /* if(hdr->vtype != f3dStructured)*/{
		fprintf(stderr, "writeF3dHeader: Unknown grid type\n");
		return -1;
	}

	abfprintf(fptr, "#!nbands \t%d\n", hdr->nbands);
	abfprintf(fptr, "#!dtype \t%s\n", (hdr->dtype == f3dCharType) ? "f3dChar" :
		(hdr->dtype == f3dUCharType) ? "f3dUChar" :
		(hdr->dtype == f3dUInt12Type) ? "f3dUInt12" :
		(hdr->dtype == f3dUInt16Type) ? "f3dUInt16" :
		(hdr->dtype == f3dInt16Type) ? "f3dInt16" :
		(hdr->dtype == f3dIntHUType) ? "f3dIntHU" :
		(hdr->dtype == f3dUInt32Type) ? "f3dUInt32" :
		(hdr->dtype == f3dInt32Type) ? "f3dInt32" :
		(hdr->dtype == f3dFloatType) ? "f3dFloat" :
		(hdr->dtype == f3dDoubleType) ? "f3dDouble" :
		"unknown");
	abfprintf(fptr, "#!units \t%s\n", (hdr->units == f3dUmm) ? "f3dUmm" :
		(hdr->units == f3dUm) ? "f3dUm" :
		(hdr->units == f3dUum) ? "f3dUum" :
		"unknown");
	if (hdr->bgtUsed) {
		if ((hdr->dtype == f3dFloatType) || (hdr->dtype == f3dDoubleType))
			abfprintf(fptr, "#!bgthreshold \t%f\n", hdr->bgtFloat);
		else
			abfprintf(fptr, "#!bgthreshold \t%d\n", hdr->bgtInt);
	}

	//obsolete since version 1.1
	//fprintf(fptr, "#!commLines \t%d\n",hdr->commentLines);

	for (i = 0; i < hdr->commentLines; i++) {
		char* tempstr = strdup(hdr->comment[i]);
		int maxidx = strlen(tempstr);
		if (maxidx > (MAX_HEADER_LINE_LEN - 1)) {
			// to have space for final 0 or '\n'
			maxidx = MAX_HEADER_LINE_LEN - 1;
			tempstr[maxidx - 1] = 0;
		}
		abfprintf(fptr, "#!comment \t%s\n",tempstr);
		free(tempstr);
	}

	if (hdr->tfnum) {
		abfprintf(fptr, "#!tfnum \t%d\n", hdr->tfnum);
		for (i = 0, k = 0; i < hdr->tfnum; i++) {
			abfprintf(fptr, "#!tfdata \td %s\n", hdr->tf[k++]);
			abfprintf(fptr, "#!tfdata \tr %s\n", hdr->tf[k++]);
			abfprintf(fptr, "#!tfdata \tg %s\n", hdr->tf[k++]);
			abfprintf(fptr, "#!tfdata \tb %s\n", hdr->tf[k++]);
			abfprintf(fptr, "#!tfdata \ta %s\n", hdr->tf[k++]);
		}
	}

	/* mark the last header line */
	abfprintf(fptr,"%s", lastcomm);
	return 0;
}

/*
*  write f3d header
*
*/
int f3dWriteHeader(
	FILE *fptr,        /* file to write in */
	const f3dHeader *hdr,        /* image header data */
	const unsigned char *img, int iw, int ih)  /* image data */
{
	if (hdr == NULL)
		return -1;

	/* magic number */
	abfprintf(fptr, "%s\n", (hdr->nbands == 1) ? "P5" : "P6");
	abfprintf(fptr, "%d %d\n", iw, ih);  /* image dimensions */
	if (f3dPrintHeader(fptr, hdr) != 0)
		return -1;
	abfprintf(fptr, "\n255\n");

	/* write icon image data */
	if(img == NULL){
		char aux = 0;
		int i;
		for(i=0; i<ih*iw * ((hdr->nbands == 1) ? 1 : 3); i++) abfwrite(&aux, 1, 1, fptr);
	} else {
		if ((int)abfwrite(img, iw * ((hdr->nbands == 1) ? 1 : 3), ih, fptr) != ih) {
			fprintf(stderr, "writeF3dHeader: File icon write error\n");
			return -1;
		}
	}
	abfflush(fptr);	/* required for streaming through named pipes */
	/*fprintf(stderr,"f3dWriteHeader(%d)\n", getpid());*/

	return 0;
}

/* RGB components should be stored separately, in order to increase the
compression ratio
*/
int f3dWriteSlice(FILE *fptr, const void *buf, int voxelSize,
	int sliceSize, f3dCompType ctype)
{
	int retval;
	if(ctype == f3dCompNone)
		retval = WriteSliceDataN(fptr, buf, voxelSize * sliceSize);
	else if (ctype == f3dCompZlib)
		retval = WriteSliceDataZ(fptr, buf, voxelSize * sliceSize);
	abfflush(fptr);	/* required for streaming through named pipes */
	/*fprintf(stderr,"f3dWriteSlice(%d)\n", getpid());*/
	return retval;
}

/*
*  return number of bytes for one voxel, header version
*/
int f3dVoxelSize(const f3dHeader *hdr)
{
	if (hdr == NULL)
		return -1;

	return f3dVoxelSizeFromType(hdr->dtype);
}
/*
*  return number of bytes for one voxel, plain type version
*/
int f3dVoxelSizeFromType(f3dDataType dtype)
{
	int  s;

	switch(dtype){
	case f3dUCharType:    s = sizeof(f3dUChar);  break;
	case f3dCharType:     s = sizeof(f3dChar);   break;
	case f3dUInt16Type:   s = sizeof(f3dUInt16); break;
	case f3dUInt12Type:   s = sizeof(f3dUInt16); break;
	case f3dInt16Type:    s = sizeof(f3dInt16);  break;
	case f3dIntHUType:    s = sizeof(f3dInt16);  break;
	case f3dUInt32Type:   s = sizeof(f3dUInt32); break;
	case f3dInt32Type:    s = sizeof(f3dInt32);  break;
	case f3dFloatType:    s = sizeof(f3dFloat);  break;
	case f3dDoubleType:   s = sizeof(f3dDouble);  break;
	default:
		fprintf(stderr,"f3dVoxelSize: unknown voxel type\n");
		return -1;
	}
	return s;
}

#define  SLICE_SUM(tt)						\
	{								\
	tt *ptr = (tt *)buf + b * *iw * *ih;			\
	float *bptr = bsum;					\
	int i;						\
	for(i=0; i< *iw * *ih; i++, bptr++, ptr++)		\
		*bptr += *ptr;				\
	}

unsigned char *f3dCreateIconImage(const void *buf, const f3dHeader *h, int *iw,
	int *ih)
{
	unsigned char *img = 0, *iptr;
	float *bsum, *bptr;
	int  i,b;
	float  vmax, vmin;


	vmax = vmin = 0;

	*iw = h->nx;
	*ih = h->ny;

	if ((bsum = MyCalloc(*iw * *ih * ((h->nbands == 1) ? 1 : 3), float)) == 0) {
		fprintf(stderr, "f3dCreateIconImage ");
		fprintf(stderr, "-- allocation error)\n");
		return NULL;
	}

	if ((img = MyCalloc(*iw * *ih * ((h->nbands == 1) ? 1 : 3), unsigned char))
		== 0)
	{
		fprintf(stderr, "f3dCreateIconImage ");
		fprintf(stderr, "-- allocation error)\n");
		free(bsum);
		return NULL;
	}

	/* compute the float image */
	for (b = 0; b < MIN(3, h->nbands); b++) {
		switch (h->dtype) {
			case f3dInt16Type:
			{
				f3dInt16 *ptr = (f3dInt16 *)buf + b * *iw * *ih;
				float *bptr = bsum;
				int i;
				for (i = 0; i < *iw * *ih; i++, bptr++, ptr++)
				*bptr += *ptr;
			}
			break;
			case f3dUCharType:  SLICE_SUM(f3dUChar) break;
			case f3dCharType:   SLICE_SUM(f3dChar) break;
			case f3dUInt16Type: SLICE_SUM(f3dUInt16) break;
			case f3dUInt12Type: SLICE_SUM(f3dUInt16) break;
			case f3dIntHUType:  SLICE_SUM(f3dInt16) break;
			case f3dUInt32Type: SLICE_SUM(f3dUInt32) break;
			case f3dInt32Type:  SLICE_SUM(f3dInt32) break;
			case f3dFloatType:  SLICE_SUM(f3dFloat) break;
			case f3dDoubleType: SLICE_SUM(f3dDouble) break;
			default:
				fprintf(stderr,"f3dCreateIconImage: unknown voxel type\n");
				free(bsum);
				free(img);
				return NULL;
		}
	}

	/* normalize and copy */
	for (bptr = bsum, i = 0; i < MIN(b, 3) * *iw * *ih; i++, bptr++) {
		float val = *bptr;
		vmax = MAX(vmax, val);
		vmin = MIN(vmin, val);
	}
	/* XXXXXXXXXX */
	for (bptr = bsum, iptr = img, i = 0; i < MIN(b, 3) * *iw * *ih;
		i++, bptr++, iptr++)
	{
		*(iptr) = (unsigned char) ((*bptr - vmin) * 255 / (vmax - vmin));
	}

	free(bsum);

	return img;
}

int f3dLibraryVersion(int* major, int* minor) {
	*major = F3D_VERSION_MAJOR;
	*minor = F3D_VERSION_MINOR;

	return 0;
}


/******************************************************************/
/*********** little endian/ big endian stuff **********************/
/******************************************************************/

/*
*  returns host byte order type
*
*/
int f3dHostType(void)
{
	int one = 1;
	char *onePtr = (char *) &one;

	if (*onePtr == 0)
		return f3dBigEndian;
	else
		return f3dLittleEndian;
}

/******************************************************************/
/*********** Header manipulation stuff ****************************/
/******************************************************************/

/* delete comment in the header */
int f3dDelHdrComment(f3dHeader *h)
{
	int i;

	if (h == NULL)
		return -1;

	for (i = 0; i < h->commentLines; i++)
		free(h->comment[i]);
	free(h->comment);
	h->comment = NULL;
	h->commentLines = 0;

	return 0;
}

/* set comment in the header */
int f3dAddHdrComment(f3dHeader *h, const char *txt)
{
	if ((h == NULL) || (txt == NULL))
		return -1;

	if ((h->comment = MyRealloc(h->comment, h->commentLines + 1, char*)) == 0) {
		fprintf(stderr, "f3dAddHdrComment: Allocation Error\n");
		return -1;
	}
	h->comment[h->commentLines] = strdup(txt);
	h->commentLines++;

	return 0;
}

/* set comment in the header */
int f3dSetHdrComment(f3dHeader *h, int n, char **txt)
{
	int i;

	if ((h == NULL) || (txt == NULL))
		return 0;

	if (h->commentLines)
		f3dDelHdrComment(h);

	if (n) {
		h->commentLines = n;
		if ((h->comment = MyCalloc(n, char *)) == 0) {
			fprintf(stderr,"f3dSetHdrComment: Allocation Error\n");
			return -1;
		}

		for (i = 0; i < n; i++) {
			if (txt[i] == NULL)
				return -1;

			h->comment[i] = strdup(txt[i]);
		}
	}

	return 0;
}

// remove one TF from the header
int f3dDelHdrTF(f3dHeader *h, int which)
{
	int i;

	if (h == NULL)
		return -1;

	if (which >= h->tfnum) {
		fprintf(stderr, "f3dDelHdrTF: Incorrect TF number\n");
		return -1;
	}

	if (h->tfnum > 1) {
		/* delete the required TF */
		for (i = which * TF_HEADER_LINES; i < (which + 1) * TF_HEADER_LINES;
			i++)
		{
			free(h->tf[i]);
		}

		/* move the rest one TF back */
		for (i = which * TF_HEADER_LINES; i < (h->tfnum - 1) * TF_HEADER_LINES;
			i++)
		{
			h->tf[i] = h->tf[i+TF_HEADER_LINES];
		}

		/* reallocate */
		h->tfnum--;
		if ((h->tf = MyRealloc(h->tf, (h->tfnum) * TF_HEADER_LINES, char *)) ==
			0)
		{
			fprintf(stderr, "f3dDelHdrTF: Allocation Error\n");
			return -1;
		}
	} else {
		h->tfnum = 0;
		for (i = 0; i < TF_HEADER_LINES; i++)
			free(h->tf[i]);
		free(h->tf);
		h->tf = NULL;
	}

	return 0;
}

// set one TF in the header
int f3dAddHdrTF(f3dHeader *h, const char **txt)
{
	int i;

	if (h == NULL)
		return -1;

	if ((h->tf = MyRealloc(h->tf, (h->tfnum + 1) * TF_HEADER_LINES, char *)) ==
		0)
	{
		fprintf(stderr, "f3dAddHdrTF: Allocation Error\n");
		return -1;
	}

	for (i = 0; i < TF_HEADER_LINES; i++)
		h->tf[h->tfnum * TF_HEADER_LINES + i] = strdup(txt[i]);

	h->tfnum += 1;

	return 0;
}

/* duplicate the header */
f3dHeader f3dDupHeader(const f3dHeader *h)
{
	int i;
	f3dHeader n;

	if (h == NULL) {
		n = f3dDefaultHeader(0, 0, 0, f3dUCharType, 0);
		return n;
	} else
		n = *h;  /* copy all members */

	/* however, array members should be allocated */
	switch (n.vtype) {
	case f3dRectZ:
		if ((n.rz = MyCalloc(h->nz, float)) == 0 ) {
			fprintf(stderr, "WriteSliceData: Compression Error\n");
			return n;
		}
		for(i = 0; i < h->nz; i++)
			n.rz[i] = h->rz[i];
	break;
	case f3dRectXYZ:
		if ((n.rx = MyCalloc(h->nx, float)) == 0 ) {
			fprintf(stderr, "f3dDupHeader: Allocation Error\n");
			return n;
		}
		for (i = 0; i < h->nx; i++)
			n.rx[i] = h->rx[i];
		if ((n.ry = MyCalloc(h->ny, float)) == 0 ) {
			fprintf(stderr, "f3dDupHeader: Allocation Error\n");
			return n;
		}
		for (i = 0; i < h->ny; i++)
			n.ry[i] = h->ry[i];
		if ((n.rz = MyCalloc(h->nz, float)) == 0 ) {
			fprintf(stderr, "f3dDupHeader: Allocation Error\n");
			return n;
		}
		for (i = 0; i < h->nz; i++)
			n.rz[i] = h->rz[i];
		/*FIXME: copy blocks */
	break;
	default:
		n.rx = n.ry = n.rz = NULL;
	}

	n.commentLines = 0;
	f3dSetHdrComment(&n, h->commentLines, h->comment);

	n.tfnum = 0;
	n.tf = NULL;
	for (i = 0; i < h->tfnum; i++)
		f3dAddHdrTF(&n, (const char**)&(h->tf[TF_HEADER_LINES * i]));

	return n;
}

/**
Create an f3dHeader structure with default settings
A C function defined in f3d.h.
@param nx x grid dimension
@param ny y grid dimension
@param nz z grid dimension
@param dtype voxel type
@param nbands data type
@return New header
*/
f3dHeader f3dDefaultHeader(int nx, int ny, int nz,f3dDataType dtype, int nbands)
{
	f3dHeader hdr;
	zeroHeader(&hdr);

	hdr.versMaj=F3D_VERSION_MAJOR;
	hdr.versMin=F3D_VERSION_MINOR;

	// volume dimensions
	hdr.nx = nx;
	hdr.ny = ny;
	hdr.nz = nz;
	// volume type
	hdr.vtype = f3dCubic;
	// spacing along axes
	hdr.px = 1;
	hdr.py = 1;
	hdr.pz = 1;
	hdr.rx = NULL;
	hdr.ry = NULL;
	hdr.rz = NULL;
	// unit type
	hdr.units = f3dUmm;
	// color type
	hdr.nbands = nbands;
	// data type
	hdr.dtype = dtype;
	// host endian type
	hdr.endian = (f3dEndianType)f3dHostType();
	// number of comment lines
	hdr.commentLines = 0;
	hdr.comment = NULL;
	hdr.tfnum = 0;
	hdr.tf = NULL;
	hdr.comptype = f3dCompZlib;

	return hdr;
}


/* duplicate the header */
int f3dDelHeader(f3dHeader *h)
{
	if (h == NULL)
		return -1;

	/* array members should be freed */
	switch (h->vtype) {
		case f3dRectZ:
			free(h->rz);
		break;
		case f3dRectXYZ:
			free(h->rx);
			free(h->ry);
			free(h->rz);
		break;
		case f3dZBlock:
			free(h->block);
		break;
		default:
			;
	}

	while (h->tfnum > 0)
		f3dDelHdrTF(h, h->tfnum - 1);

	return f3dDelHdrComment(h);
}


/* get minimum possible value of the given type*/
float f3dMinT(f3dDataType dtype)
{
	switch(dtype){
	case f3dUCharType:    /* unsigned char */
		return f3dUCharMin;
	break;
	case f3dCharType:    /* char */
		return f3dCharMin;
	break;
	case f3dUInt12Type:    /* unsigned short */
		return f3dUInt12Min;
	break;
	case f3dUInt16Type:    /* unsigned short */
		return f3dUInt16Min;
	break;
	case f3dIntHUType:    /* signed short */
		return f3dIntHUMin;
	break;
	case f3dInt16Type:    /* signed short */
		return f3dInt16Min;
	break;
	case f3dUInt32Type:    /* signed int */
		return f3dUInt32Min;
	break;
	case f3dInt32Type:    /* unsigned int */
		return f3dInt32Min;
	break;
	case f3dFloatType:    /* float */
		return f3dFloatMin;
	break;
	case f3dDoubleType:    /* float */
		return f3dDoubleMin;
	break;
	}
	return 0;
}

/* get minimum possible value of the given type*/
float f3dMaxT(f3dDataType dtype)
{
	switch(dtype){
	case f3dUCharType:    /* unsigned char */
		return f3dUCharMax;
	break;
	case f3dCharType:    /* char */
		return f3dCharMax;
	break;
	case f3dUInt12Type:    /* unsigned short */
		return f3dUInt12Max;
	break;
	case f3dUInt16Type:    /* unsigned short */
		return f3dUInt16Max;
	break;
	case f3dIntHUType:    /* signed short */
		return f3dIntHUMax;
	break;
	case f3dInt16Type:    /* signed short */
		return f3dInt16Max;
	break;
	case f3dUInt32Type:    /* signed int */
		return (float)f3dUInt32Max;
	break;
	case f3dInt32Type:    /* unsigned int */
		return (float)f3dUInt32Max;
	break;
	case f3dFloatType:    /* float */
		return f3dFloatMax;
	break;
	case f3dDoubleType:    /* float */
		return f3dDoubleMax;
	break;
	}
	return 0;
}

/*********************************************************************/
/*********************************************************************/
/*******************     STATIC  FUNCTIONS     ***********************/
/*********************************************************************/
/*********************************************************************/

/*
*  reads header of the f3d file
*
*  char *path:  file path+name
*  f3dHeader *hdr: returned header
*  int *nx : icon row length
*  int *ny : icon height
*  int *color: true is color icon
*  return value: file pointer, pointer set to the start of the icon image
*/
#define PGM 6
#define PPM 5
static FILE *ReadHeader(FILE *fp, f3dHeader *hdr, int *nx, int *ny, int *color)
{
	// to compensate for the keyword length
	int hdrLLength = MAX_HEADER_LINE_LEN + 50;
	char strval[MAX_HEADER_LINE_LEN + 50], line[MAX_HEADER_LINE_LEN + 50];
	int rxCnt = 0, ryCnt = 0, rzCnt = 0;  /* coordinate counters */
	int blockCnt = 0; /*block counter*/
	float faux, faux2, faux3;
	int pnmType, iaux;
	char caux;
	int tfcnt = 0, rgbacnt = 0; /* band and TF counters for TF reading */
	int useCommaFormat = 0;
	float cf,df;

	if ((fp == NULL) || (hdr == NULL) || (nx == NULL) || (ny == NULL) ||
		(color == NULL))
	{
		return NULL;
	}

	sscanf("0.5", "%f", &df);
	sscanf("0,5", "%f", &cf);
	if (df == 0.5) {
		/*fprintf(stderr,"Using dot format\n");*/
	} else if (cf == 0.5) {
		fprintf(stderr, "Incorrect format of floats. Set environmet variable"
			" LC_NUMERIC to en_US \n");
		useCommaFormat = 1;
	} else {
		fprintf(stderr, "Unrecognized float format\n");
		return NULL;
	}
	/* get the type of the PNM */
	abfgets(line, hdrLLength, fp);
	if (sscanf(line, "P%d", &pnmType) != 1) {
		errorCode = F3D_PNM_HEADER_ERROR;
		return NULL;
	}

	if (!(pnmType == PPM || pnmType == PGM)) {
		errorCode = F3D_INCORR_PNM_TYPE;
		return NULL;
	}
	*color = (pnmType == PGM) ? 1 : 0;

	/* get width and height */
	abfgets(line, hdrLLength, fp);
	if (sscanf(line, "%d %d", nx, ny) != 2) {
		errorCode = F3D_NOT_F3D_FORMAT;
		return NULL;
	}

	zeroHeader(hdr);
	hdr->comptype = f3dCompZlib;

	/* read the version.  */
	abfgets(line, hdrLLength, fp);
	if (sscanf(line, "#!f3d %d.%d", &hdr->versMaj, &hdr->versMin) != 2) {
		errorCode = F3D_NOT_F3D_FORMAT;
		return NULL;
	}

	if (hdr->versMaj > FORMAT_VERSION) {
		errorCode = F3D_NOT_CURRENT;
		return NULL;
	}

	while (strncmp(line, lastcomm, strlen(lastcomm) - 1)) {

		/* replace dot by comma, if using comma float format -- necessary for wxWidgets */
		/*
		if(useCommaFormat){
			if(strstr(line, "#!px") || strstr(line, "#!py") || strstr(line, "#!pz")){
				char *dptr;
				if(dptr = strchr(line, '.'))
					*dptr = ',';
			fprintf(stderr,"%s\n",line);
			}
		}
		*/

		if (abfgets(line, hdrLLength, fp) == NULL) {
			errorCode = F3D_READ_HDR_LINE_ERROR;
			return NULL;
		}


		if (sscanf(line, "#!vdim %d %d %d", &(hdr->nx), &(hdr->ny), &(hdr->nz))
			== 3)
		{
			// no code
		} else if (sscanf(line, "#!compression %s", strval) == 1) {
			if (!strcmp(strval, "zlib"))
				hdr->comptype = f3dCompZlib;
			else if(!strcmp(strval, "none"))
				hdr->comptype = f3dCompNone;
			else {
				errorCode = F3D_INCORR_ENDIAN_VAL;
				return NULL;
			}
		} else if (sscanf(line, "#!endian %s", strval) == 1) {
			if (!strcmp(strval, "f3dBigEndian"))
				hdr->endian = f3dBigEndian;
			else if (!strcmp(strval, "f3dLittleEndian"))
				hdr->endian = f3dLittleEndian;
			else {
				errorCode = F3D_INCORR_ENDIAN_VAL;
				return NULL;
			}
		} else if (sscanf(line, "#!vtype %s", strval) == 1) {
			if (!strcmp(strval, "f3dCubic"))
				hdr->vtype = f3dCubic;
			else if (!strcmp(strval, "f3dRegular"))
				hdr->vtype = f3dRegular;
			else if (!strcmp(strval, "f3dRectZ"))
				hdr->vtype = f3dRectZ;
			else if (!strcmp(strval, "f3dRectXYZ"))
				hdr->vtype = f3dRectXYZ;
			//novy typ objemu:
			else if (!strcmp(strval, "f3dZBlock"))
				hdr->vtype = f3dZBlock;
			/*else if(!strcmp(strval, "f3dStructured")) hdr->vtype = f3dStructured;*/
			else {
				errorCode = F3D_INCORR_VTYPE_VAL;
				return NULL;
			}

			hdr->rx = hdr->ry = hdr->rz = 0;
			switch (hdr->vtype) {
				case f3dRectXYZ:
					hdr->rx = MyCalloc(hdr->nx, float);
					rxCnt = 0;
					hdr->ry = MyCalloc(hdr->ny, float);
					ryCnt = 0;
				case f3dRectZ:
					hdr->rz = MyCalloc(hdr->nz, float);
					rzCnt = 0;
				break;
				default:
					;
			}
		} else if (sscanf(line, "#!ctype %s", strval) == 1) {
			// obsolete from f3d-3.0

			if (!strcmp(strval, "f3dMono"))
				hdr->nbands = 1;
			else if (!strcmp(strval, "f3dColor"))
				hdr->nbands = 3;
			else {
				errorCode = F3D_INCORR_CTYPE_VAL;
				return NULL;
			}
		} else if (sscanf(line, "#!nbands %d", &iaux) == 1) {
			if (iaux > 0){
				hdr->nbands = iaux;
			} else {
				errorCode = F3D_INCORR_NBANDS_VAL;
				return NULL;
			}
		} else if (sscanf(line, "#!dtype %s", strval) == 1) {
			if (!strcmp(strval, "f3dChar")) hdr->dtype = f3dCharType;
			else if(!strcmp(strval,"f3dUChar")) hdr->dtype = f3dUCharType;
			else if(!strcmp(strval,"f3dUInt16")) hdr->dtype = f3dUInt16Type;
			else if(!strcmp(strval,"f3dUInt12")) hdr->dtype = f3dUInt12Type;
			else if(!strcmp(strval,"f3dInt16")) hdr->dtype = f3dInt16Type;
			else if(!strcmp(strval,"f3dIntHU")) hdr->dtype = f3dIntHUType;
			else if(!strcmp(strval,"f3dUInt32")) hdr->dtype = f3dUInt32Type;
			else if(!strcmp(strval,"f3dInt32")) hdr->dtype = f3dInt32Type;
			else if(!strcmp(strval,"f3dFloat")) hdr->dtype = f3dFloatType;
			else if(!strcmp(strval,"f3dDouble")) hdr->dtype = f3dDoubleType;
			else {
				errorCode = F3D_INCORR_DTYPE_VAL;
				return NULL;
			}
		} else if (sscanf(line,"#!units %s", strval) == 1) {
			if(!strcmp(strval, "f3dUmm")) hdr->units = f3dUmm;
			else if(!strcmp(strval, "f3dUm")) hdr->units = f3dUm;
			else if(!strcmp(strval, "f3dUum")) hdr->units = f3dUum;
			else {
				errorCode = F3D_INCORR_UNITS_VAL;
				return NULL;
			}
		} else if (!strncmp(line, "#!bgthreshold", 5)) {
			if ((hdr->dtype == f3dFloatType) || (hdr->dtype == f3dDoubleType)) {
				if (sscanf(line, "#!bgthreshold %f", &faux) == 1) {
					hdr->bgtFloat = faux;
					hdr->bgtUsed = 1;
				}
			} else {
				if (sscanf(line, "#!bgthreshold %d", &iaux) == 1) {
					hdr->bgtInt = iaux;
					hdr->bgtUsed = 1;
				}
			}
		} else if (!strncmp(line, "#!comment", 9)) {
			char *p;

			line[strlen(line) - 1] = 0;  /* delete the \n character */
			p = line + strlen("#!comment");
			while (isspace(*p))
				p++;
			f3dAddHdrComment(hdr, p);
		} else if (sscanf(line,"#!px %f", &faux) == 1) {
			if (hdr->vtype == f3dRectXYZ) {
				hdr->rx[rxCnt++] = faux;
			} else {
				/*fprintf(stderr,"px: %g\n", faux);*/
				hdr->px = faux;
				if (hdr->vtype == f3dCubic) {
					hdr->py = faux;
					hdr->pz = faux;
				}
			}
		} else if (sscanf(line, "#!py %f", &faux) == 1) {
			if (hdr->vtype == f3dRectXYZ)
				hdr->ry[ryCnt++] = faux;
			else
				hdr->py = faux;
		} else if (sscanf(line,"#!pz %f", &faux) == 1) {
			if (hdr->vtype == f3dRectXYZ || hdr->vtype == f3dRectZ)
				hdr->rz[rzCnt++] = faux;
			else
				hdr->pz = faux;
		} else if (sscanf(line,"#!nblocks %d", &iaux) == 1) {
			hdr->nblocks = iaux;
			/* allocate the blocks here */
			hdr->block = MyCalloc (hdr->nblocks, ZBlock);
			/*FIXME: test pointer validity */

		} else if (sscanf(line, "#!block %f %f %f %d", &faux, &faux2, &faux3,
			&iaux) == 4)
		{//for vtype f3dZBlock
			hdr->block[blockCnt].blockpx = faux; //voxel size in x axis
			hdr->block[blockCnt].blockpy = faux2; //voxel size in y axis
			hdr->block[blockCnt].blockpz = faux3; //voxel size in z axis
			hdr->block[blockCnt].blocknz = iaux; //number of slices in a block
			blockCnt++; //increase number of blocks
		} else if (sscanf(line,"#!tfnum %d", &iaux) == 1) {
			hdr->tfnum = iaux;
			hdr->tf = MyCalloc(hdr->tfnum * TF_HEADER_LINES, char*);
			if (hdr->tf == NULL) {
				errorCode = F3D_TF_ALLOC;
				return NULL;
			}
		} else if (sscanf(line, "#!tfdata %c %[ \t.0-9_a-zA-Z-] ", &caux,
			&strval[0]) == 2)
		{
			if (hdr->tfnum == 0) {
				errorCode = F3D_TFNUM_MISS;
				return NULL;
			}

			if (!(hdr->tf[TF_HEADER_LINES*tfcnt + rgbacnt] = strdup(strval))) {
				errorCode = F3D_TF_ALLOC;
				return NULL;
			}

			/* update counters */
			if (++rgbacnt == TF_HEADER_LINES) {
				rgbacnt = 0;
				tfcnt++;
			}
		}
	}

	/* test the number of loaded transfer functions */
	//if(hdr->tfnum && bcnt < hdr->nbands){
	//errorCode = F3D_TF_FEW;
	//return NULL;
	//}

	/* read the PNM maxval field and ignore it */
	abfgets(line, hdrLLength, fp);

	return (checkHeader(hdr) == 0 ) ? fp : NULL;
}


/*
*   skip slice data
*/
static int SkipSliceData(FILE *fp)
{
	uLong sl;
	char b;
	int i;

	if (fp == NULL)
		return -1;

	sl = ReadLong(fp);
	for(i = 0; i < sl; i++)
		if (abfread(&b, 1, 1, fp) !=1) {
			//fprintf(stderr, "ReadSliceDataN: read error (trying to read 1 byte" " of data)\n");
			return -1;
		}

	return 0;
}

/*
*  read uncompressed slice data
*/
static int ReadSliceDataN(FILE *fp, void *ptr, unsigned long size)
{
	uLong sl;
	int readerr=0;

	if ((fp == NULL) || (ptr == NULL))
		return -1;

	sl = ReadLong(fp);
	if (ISFILE(fp)) { /*regular file, read byte-by-byte*/
		if (abfread(ptr, 1, sl, fp) != sl)
			readerr = 1;
	} else { /* thb, read entire block at once*/
		if (abfread(ptr, sl, 1, fp) != 1)
			readerr=1;
	}

	if (readerr) {
		//fprintf(stderr, "ReadSliceDataN: read error (trying to read %lu bytes" " of data)\n", sl);
		return -1;
	}

	return 0;
}

/* write uncompressed slice data */
static int WriteSliceDataN(FILE *fptr, const void *buf, unsigned long size)
{
	int wrerr = 0;

	if ((fptr == NULL) || (buf == NULL))
		return -1;

	/* write now */
	WriteLong(fptr, (long)size);

	if (ISFILE(fptr)) { /*regular file, write byte-by-byte*/
		if (abfwrite(buf, 1, size, fptr) != size)
			wrerr=1;
	} else { /* thb, write entire block at once*/
		if (abfwrite(buf, size, 1, fptr) != 1)
			wrerr=1;
	}

	if (wrerr) {
		//fprintf(stderr, "WriteSliceDataN: Write Error\n");
		return -1;
	}
	/*if (abfwrite(buf, 1, size, fptr) != size){
		fprintf(stderr,"WriteSliceDataN: Write Error\n");
	}*/

	return 0;
}

/*
*  read and decompress slice data
*/
static int ReadSliceDataZ(FILE *fp, void *ptr, unsigned long size)
{
	uLong sl;
	uLongf dl;
	Bytef *ibuf;
	int errc = Z_OK;
	int readerr = 0;

	if ((fp == NULL) || (ptr == NULL))
		return -1;

	sl = ReadLong(fp);
	if ((ibuf = MyCalloc(sl, Bytef)) == 0) {
		//fprintf(stderr, "ReadSliceDataZ: Allocation error.\n");
		return -1;
	}

	if (ISFILE(fp)) { /*regular file, read byte-by-byte*/
		if (abfread(ibuf, 1, sl, fp) != sl)
			readerr=1;
	} else { /* thb, read entire block at once*/
		if (abfread(ibuf, sl, 1, fp) != 1)
			readerr=1;
	}

	if (readerr) {
		//fprintf(stderr, "ReadSliceDataZ: read error (trying to read %lu bytes" " of data)\n", sl);
		free(ibuf);
		return -1;
	}
	/*if (abfread(ibuf, 1, sl, fp) != 1){
		fprintf(stderr,"ReadSliceDataZ: read error (trying to read %d bytes of data)\n", sl);
	}*/

	dl = size;
	errc = uncompress((Bytef *)ptr, &dl, ibuf, sl);
	if (errc != Z_OK) {
		fprintf(stderr,"ReadSliceDataZ: Uncompression Error (%d)\n", errc);
		free(ibuf);
		return -1;
	}

	if (dl != size){
		fprintf(stderr, "ReadSliceDataZ: Incorrect data length (%lu, %lu)\n",
			dl, size);
		free(ibuf);
		return -1;
	}

	free(ibuf);
	return 0;
}

static int WriteSliceDataZ(FILE *fptr, const void *buf, unsigned long size)
{
	Bytef *cbuf;    /* compression buffer */
	uLongf dl, destLen = (uLongf)(1.01 * size + 12);
	int wrerr = 0;

	if ((fptr == NULL) || (buf == NULL))
		return -1;

	/* constants from zlib.h */
	if ((cbuf = MyCalloc(1.01 * size + 12, Bytef)) == NULL) {
		fprintf(stderr, "WriteSliceDataZ: Allocation Error\n");
		return -1;
	}

	dl = destLen;
	if (compress((Bytef *)cbuf, &dl, (Bytef *)buf, (uLong)size) != Z_OK) {
		fprintf(stderr,"WriteSliceDataZ: Compression Error\n");
		free(cbuf);
		return -1;
	}

	/* write now */
	WriteLong(fptr, (long)dl);

	if (ISFILE(fptr)) { /*regular file, write byte-by-byte*/
		if (abfwrite(cbuf, 1, dl, fptr) != dl)
			wrerr = 1;
	} else { /* thb, write entire block at once*/
		if (abfwrite(cbuf, dl, 1, fptr) != 1)
			wrerr = 1;
	}
	if (wrerr) {
		fprintf(stderr,"WriteSliceDataZ: Write Error\n");
		free(cbuf);
		return -1;
	}
	/*if (abfwrite(cbuf, 1, dl, fptr) != dl){
		fprintf(stderr,"WriteSliceDataZ: Write Error\n");
	}*/

	free(cbuf);
	return 0;
}

/* ========================================================================== */

/*
*  write long integer, Most Significant Byte first
*/
static int WriteLong(FILE *fptr, int l)
{
	unsigned char ab[4];

	long m0 = 0x000000ffl;
	long m1 = 0x0000ff00l;
	long m2 = 0x00ff0000l;
	long m3 = 0xff000000l;

	if (fptr == NULL)
		return -1;

	/* write, MSB first */
	/*
	w = (unsigned char)((l&m3)>>24); abfwrite((void *)&w,1,1,fptr);
	w = (unsigned char)((l&m2)>>16); abfwrite((void *)&w,1,1,fptr);
	w = (unsigned char)((l&m1)>>8); abfwrite((void *)&w,1,1,fptr);
	w = (unsigned char)((l&m0)); abfwrite((void *)&w,1,1,fptr);
	*/
	ab[0] = (unsigned char)((l&m3)>>24);
	ab[1] = (unsigned char)((l&m2)>>16);
	ab[2] = (unsigned char)((l&m1)>>8);
	ab[3] = (unsigned char)((l&m0));
	abfwrite((void *)ab, 4, 1, fptr);

	return 0;
}

/*
*  read long integer, Most Significant Byte first
*/
static long ReadLong(FILE *fptr)
{
	/*unsigned char w;*/
	unsigned char x[4];
	long ret = 0;

	if (fptr == NULL)
		return -1;

	/*if(abfread((void *)&w,1,1,fptr) != 1){
		fprintf(stderr,"ReadLong error (1)\n");
		exit(1);
	}
	ret |= w; ret <<= 8;
	if(abfread((void *)&w,1,1,fptr) != 1){
		fprintf(stderr,"ReadLong error (2)\n");
		exit(1);
	}
	ret |= w; ret <<= 8;
	if(abfread((void *)&w,1,1,fptr) != 1){
		fprintf(stderr,"ReadLong error (3)\n");
		exit(1);
	}
	ret |= w; ret <<= 8;
	if(abfread((void *)&w,1,1,fptr) != 1){
		fprintf(stderr,"ReadLong error (4)\n");
		exit(1);
	}
	ret |= w;
	*/
	if (abfread((void *)x,1,4,fptr) != 4) {
		//fprintf(stderr,"ReadLong error\n");
		return -1;
	}
	ret |= x[0]; ret <<= 8;
	ret |= x[1]; ret <<= 8;
	ret |= x[2]; ret <<= 8;
	ret |= x[3];

	return ret;
}


/*
*  swap byte order, if file and host type do mot match
*/
static int SwapOrder(unsigned char *b, int l)
{
	unsigned char c;

	if (b == NULL)
		return -1;

	switch (l) {
	case 1:
		/* do nothing */
	break;
	case 2:
		c = b[0]; b[0] = b[1]; b[1] = c;
	break;
	case 4:
		c = b[0]; b[0] = b[3]; b[3] = c;
		c = b[1]; b[1] = b[2]; b[2] = c;
	break;
	case 8:
		c = b[0]; b[0] = b[7]; b[7] = c;
		c = b[1]; b[1] = b[6]; b[6] = c;
		c = b[2]; b[2] = b[5]; b[5] = c;
		c = b[3]; b[3] = b[4]; b[4] = c;
	break;
	default:
		fprintf(stderr, "SwapOrder:: Incorrect item size.\n");
		return -1;
	break;
	}

	return 0;
}

/*
*  set all members of the f3d header structure to 0
*/
static int zeroHeader(f3dHeader *hdr)
{
	if (hdr == NULL)
		return -1;

	hdr->comptype = (f3dCompType)0;
	hdr->endian = (f3dEndianType)0;
	hdr->nx = 0;
	hdr->ny = 0;
	hdr->nz = 0;
	hdr->vtype = (f3dGridType)0;
	hdr->px = 0;
	hdr->py = 0;
	hdr->pz = 0;
	hdr->rx = 0;
	hdr->ry = 0;
	hdr->rz = 0;
	hdr->nblocks = 0;
	hdr->block = 0;
	hdr->units = (f3dUnitType)0;
	hdr->nbands = 0;
	hdr->dtype = (f3dDataType)0;
	hdr->commentLines = 0;
	hdr->comment = 0;
	hdr->tfnum = 0;
	hdr->tf = NULL;
	hdr->bgtUsed = 0;
	hdr->bgtFloat = 0;
	hdr->bgtInt = 0;

	return 0;
}

/*
*  set all members of the f3d header structure to 0
*/
static int checkHeader(f3dHeader *hdr)
{
	float s;
	int  i;

	if (hdr == NULL)
		return -1;

	if (hdr->comptype == 0) {
		errorCode = F3D_COMP_MISS;
		return -1;
	}
	if (hdr->endian == 0) {
		errorCode = F3D_END_MISS;
		return -1;
	}
	if ( hdr->nx == 0 || hdr->ny == 0 || hdr->nz == 0) {
		errorCode = F3D_INCORR_DIM;
		return -1;
	}
	if (hdr->vtype == 0) {
		errorCode = F3D_VTYPE_MISS;
		return -1;
	}
	if (hdr->units == 0) {
		errorCode = F3D_UNITS_MISS;
		return -1;
	}
	if (hdr->nbands == 0) {
		errorCode = F3D_NBANDS_MISS;
		return -1;
	}
	if (hdr->dtype == 0) {
		errorCode = F3D_DTYPE_MISS;
		return -1;
	}
	if (hdr->commentLines > 0)
		if (hdr->comment == 0) {
		errorCode = F3D_COMM_MISS;
		return -1;
	}

	switch(hdr->vtype) {
	case f3dRegular:
		if (hdr->pz < 1e-20 ) {
		errorCode = F3D_PZ_MISS;
		return -1;
		}
		if (hdr->py < 1e-20 ) {
		errorCode = F3D_PY_MISS;
		return -1;
		}
	case f3dCubic:
		if (hdr->px < 1e-20 ) {
		errorCode = F3D_PX_MISS;
		return -1;
		}
	break;
	case f3dRectZ:
		if (hdr->px < 1e-20 ) {
		errorCode = F3D_PX_MISS;
		return -1;
		}
		if (hdr->py < 1e-20 ) {
		errorCode = F3D_PY_MISS;
		return -1;
		}
		s = 0;
		for(i=0; i<hdr->nz; i++) {
		if (hdr->rz[i] < 1e-20) {
			errorCode = F3D_RZ_ZERO;
			return -1;
		}
		s += hdr->rz[i];
		}
		if (s < 1e-20 ) {
		errorCode = F3D_RZ_MISS;
		return -1;
		}
	break;
	case f3dZBlock:
		for(i=0; i<hdr->nblocks; i++) {
		if (hdr->block[i].blockpx < 1e-20) {
		// f3dErrorCode = F3D_BLOCKPX_MISS;
			return -1;
		}
		if (hdr->block[i].blockpy < 1e-20) {
		// f3dErrorCode = F3D_BLOCKPY_MISS;
			return -1;
		}
		if (hdr->block[i].blockpz < 1e-20) {
		// f3dErrorCode = F3D_BLOCKPZ_MISS;
			return -1;
		}
		}
		if (hdr->nblocks == 0 ) {
		//f3dErrorCode = F3D_NBLOCKS_MISS;
		return -1;
		}
	break;

	case f3dRectXYZ:
		s = 0;
		for(i=0; i<hdr->nx; i++) {
		if (hdr->rx[i] < 1e-20) {
			errorCode = F3D_RZ_ZERO;
			return -1;
		}
		s += hdr->rx[i];
		}
		if (s < 1e-20 ) {
		errorCode = F3D_RY_MISS;
		return -1;
		}
		s = 0;
		for(i=0; i<hdr->ny; i++) {
		if (hdr->ry[i] < 1e-20) {
			errorCode = F3D_RZ_ZERO;
			return -1;
		}
		s += hdr->ry[i];
		}
		if (s < 1e-20 ) {
		errorCode = F3D_RY_MISS;
		return -1;
		}
		s = 0;
		for(i=1; i<hdr->nz; i++) {
		if (hdr->rz[i] < 1e-20) {
			errorCode = F3D_RZ_ZERO;
			return -1;
		}
		s += hdr->rz[i];
		}
		if (s < 1e-20 ) {
		errorCode = F3D_RZ_MISS;
		return -1;
		}
		/*FIXME: pritad ZBlock */
	break;
	}
	return 0;
}
